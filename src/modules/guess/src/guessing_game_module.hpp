//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "commands/guess_command.hpp"

#include <core/module/module.hpp>
#include <utility/close_scheduler.hpp>

using namespace std::string_view_literals;

namespace modules::guess
{

class guessing_game_module : public core::module::module<guessing_game_module>
{
public:
  explicit guessing_game_module(boost::asio::io_service &io)
    : core::module::module<guessing_game_module>{io},
      close_scheduler_{io}
  { }

  std::string_view get_name() override
  {
    return "bet"sv;
  }

  std::string_view get_version() override
  {
    return "0.1.22"sv;
  }

  void create() override
  {
    register_command<commands::guess_command>();
  }

  bool has_bet() const
  { return static_cast<bool>(bet_); }

  bet& get_bet()
  { return *(bet_.get()); }

  void reset_bet()
  {
    bet_ = std::make_unique<bet>();
  }

  utility::close_scheduler& get_close_scheduler()
  { return close_scheduler_; }

private:
  std::unique_ptr<bet> bet_;
  utility::close_scheduler close_scheduler_;
};

}
