//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "../irc/context.hpp"

#include <tuple>

namespace core::module
{

template<bool, typename A, typename B>
struct either_or
{
  using type = A;
};

template<typename A, typename B>
struct either_or<false, A, B>
{
  using type = B;
};

template<bool Condition, typename A, typename B>
using either_or_t = typename either_or<Condition, A, B>::type;

template<bool Condition, typename T>
using add_lvalue_reference_if_t = either_or_t<Condition, std::add_lvalue_reference_t<T>, T>;

template<typename T>
using argument_passing_t = either_or_t<
  std::is_fundamental_v<T>,
  T,
  std::add_lvalue_reference_t<std::add_const_t<T>>>;

template<bool, typename T, typename... As>
struct handler_function_type_impl
{
  using function_type = bool (T::*) (const irc::context &, argument_passing_t<As>...);
};

template<typename T, typename... As>
struct handler_function_type_impl<true, T, As...>
{
  using function_type = bool (T::*) (const irc::context &, argument_passing_t<As>..., const std::vector<std::string>&);
};

template<bool CaptureRemaining, typename T, typename... As>
using handler_function_t = typename handler_function_type_impl<CaptureRemaining, T, As...>::function_type;

template<typename... Ts>
class args
{
public:
  using tuple = std::tuple<Ts...>;

  template<typename Command>
  bool apply(
    bool (Command::*handler_function)(const irc::context &, argument_passing_t<Ts>...),
    Command &command,
    const irc::context &context) const
  {
    return std::apply(
      [&](auto &&... values) {
        return (command.*handler_function)(context, std::forward<decltype(values)>(values)...);
      },
      values_);
  }

  bool is_valid() const
  { return is_valid_; }

  void is_valid(bool valid)
  { is_valid_ = valid; }

  template<std::size_t I>
  void set_value(const std::tuple_element_t<I, tuple> &value)
  {
    std::get<I>(values_) = value;
  }

private:
  bool is_valid_ = false;
  tuple values_;
};

}
