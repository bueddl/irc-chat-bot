//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/asio/ssl/rfc2818_verification.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

namespace core::bot::detail
{

template<typename Socket>
class connect_strategy
{
public:
  using socket_type = Socket;

  explicit connect_strategy(boost::asio::io_service &context)
    : socket_{context}
  { }

  socket_type &socket()
  { return socket_; }

  void connect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
    std::function<void(const boost::system::error_code &ec)> handle_connect)
  {
    handle_connect_ = std::move(handle_connect);

    boost::asio::async_connect(socket_.lowest_layer(), endpoint_iterator,
      boost::bind(handle_connect_,
        boost::asio::placeholders::error));
  }

private:
  socket_type socket_;

  std::function<void(const boost::system::error_code &ec)> handle_connect_;
};

template<typename NextLayerSocket>
class connect_strategy<boost::asio::ssl::stream<NextLayerSocket>>
{
public:
  using socket_type = boost::asio::ssl::stream<NextLayerSocket>;

  explicit connect_strategy(boost::asio::io_service &context)
    : context_{boost::asio::ssl::context::sslv23},
      socket_{context, context_}
  {
    context_.set_default_verify_paths();
    socket_.set_verify_mode(boost::asio::ssl::verify_peer);
  }

  socket_type &socket()
  { return socket_; }

  void connect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator,
    std::function<void(const boost::system::error_code &ec)> handle_connect)
  {
    socket_.set_verify_callback(
      boost::asio::ssl::rfc2818_verification(
        endpoint_iterator->host_name()));

    handle_connect_ = std::move(handle_connect);

    boost::asio::async_connect(socket_.lowest_layer(), endpoint_iterator,
      boost::bind(&connect_strategy::handle_connect, this,
        boost::asio::placeholders::error));
  }

private:
  boost::asio::ssl::context context_;
  socket_type socket_;

  std::function<void(const boost::system::error_code &ec)> handle_connect_;

  void handle_connect(const boost::system::error_code &ec)
  {
    if (ec)
      return;

    socket_.async_handshake(boost::asio::ssl::stream_base::client,
      boost::bind(handle_connect_,
        boost::asio::placeholders::error));
  }
};


}
