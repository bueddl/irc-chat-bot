//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "module.hpp"

namespace core::module
{

class module_manager
{
public:
  explicit module_manager(boost::asio::io_service &io)
    : io_{io}
  { }

  template<typename T, typename... Args>
  std::shared_ptr<T> register_module(Args &&... args)
  {
    auto module = std::make_shared<T>(io_, std::forward<Args>(args)...);
    modules_.emplace_front(module);

    std::cout << "Module loaded: "
              << module->get_name()
              << " (version: "
              << module->get_version()
              << ")"
              << std::endl;

    return module;
  }

  void release_all()
  {
    while (!modules_.empty()) {
      auto &module = modules_.front();
      modules_.pop_front();
      module->disable();
    }
  }

  void on_message(const irc::context &context, const irc::protocol::message &message)
  {
    for (auto &module : modules_)
      module->on_message(context, message);
  }

private:
  boost::asio::io_service &io_;

  std::forward_list<std::shared_ptr<module_base>> modules_;
};

}
