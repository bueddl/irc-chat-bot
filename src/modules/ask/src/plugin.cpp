//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "ask_module.hpp"

#include <core/plugin_system/module_plugin.hpp>

namespace modules::ask
{

class ask_plugin : public core::plugin_system::module_plugin<ask_module>
{
};

}

std::shared_ptr<core::plugin_system::plugin> make_plugin()
{
  return std::make_shared<modules::ask::ask_plugin>();
}
