//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <core/module/command.hpp>
#include <twitch/usertype_authorizer.hpp>

#include <chrono>

namespace modules::poll::commands
{

template<typename Module>
class poll_command : public core::module::command<poll_command<Module>, Module>
{
public:
  using core::module::command<poll_command, Module>::command;

  std::string_view get_name() const override
  {
    return getenv("CHATBOT_MOD_POLL_MOD_COMMAND");
  }

  bool enable() override
  {
    this->add_syntax(
      &poll_command::display_help);

    this->add_syntax_with_capture(
      "open",
      &poll_command::open,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax_with_capture<int>(
      "open <timeout>",
      &poll_command::open,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->add_syntax(
      "reopen",
      &poll_command::reopen,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<uint>(
      "reopen <delay>",
      &poll_command::reopen,
      twitch::usertype_authorizer{twitch::usertype::mod});

    this->add_syntax(
      "close",
      &poll_command::close,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<uint>(
      "close <delay>",
      &poll_command::close,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->add_syntax(
      "close cancel",
      &poll_command::cancel_close,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->add_syntax(
      "close now",
      &poll_command::close_now,
      twitch::usertype_authorizer{twitch::usertype::mod});

    this->add_syntax(
      "results",
      &poll_command::display_results,
      twitch::usertype_authorizer{twitch::usertype::mod});

    this->add_syntax(
      "options",
      &poll_command::show_options,
      twitch::usertype_authorizer{twitch::usertype::mod});

    return true;
  }

private:
  bool display_help(const core::irc::context &context)
  {
    std::ostringstream message;
    message << "Vote for the option you prefer by using the !vote <option-no> command.";

    auto &poll = this->get_module().get_poll();
    if (this->get_module().has_poll() && poll.is_open())
      message << "Use either !vote 1 or !vote"
              << poll.get_options()[0]
              << " to vote for the first option.";

    context.channel_message(message);
    return true;
  }

  bool open(const core::irc::context &context, int close_delay, const std::vector<std::string> &args)
  {
    // Is there a poll still in progress?
    if (this->get_module().has_poll() && this->get_module().get_poll().is_open()) {
      context.channel_message_to_user("There is still a poll in progress!");
      return false;
    }

    if (close_delay < 0) {
      context.channel_message_to_user("Timeout must be positive.");
      return false;
    }

    std::string text;
    auto arg_iterator = std::begin(args);
    for (; arg_iterator != std::end(args) && arg_iterator->at(0) != '*'; ++arg_iterator) {
      if (!text.empty())
        text += ' ';
      text += *arg_iterator;
    }

    if (text.empty()) {
      context.channel_message_to_user("Empty subject not allowed.");
      return false;
    }

    std::vector<std::string> options;
    while (arg_iterator != std::end(args)) {
      std::string option = arg_iterator->substr(1);
      ++arg_iterator;
      for (; arg_iterator != std::end(args) && arg_iterator->at(0) != '*'; ++arg_iterator)
        option += ' ' + *arg_iterator;

      if (option.empty()) {
        context.channel_message_to_user("Empty option not allowed.");
        return false;
      }

      if (std::find(std::begin(options), std::end(options), option) != std::end(options)) {
        context.channel_message_to_user("Options must be unique, duplicate option: " + option + ".");
        return false;
      }
      options.emplace_back(option);
    }

    if (options.empty()) {
      context.channel_message_to_user("No options given.");
      return false;
    }

    this->get_module().create_poll(text, options);

    auto &poll = this->get_module().get_poll();
    poll.open();

    context.channel_message("✈ ✈ ✈ New poll: " + text, true);
    show_options(context);
    if (close_delay > 0)
      close(context, close_delay);

    return true;
  }

  bool open(const core::irc::context &context, const std::vector<std::string> &args)
  {
    return open(context, 0, args);
  }

  bool reopen(const core::irc::context &context, uint close_delay)
  {
    auto &poll = this->get_module().get_poll();

    // If there is no poll in progress, tell the invoking user.
    if (!this->get_module().has_poll()) {
      context.channel_message_to_user("There is no poll taking place right now.");
      return false;
    }

    // Check if the poll is still open
    if (poll.is_open()) {
      context.channel_message_to_user("This poll is still open.");
      return true;
    }

    poll.reopen();
    if (close_delay > 0)
      close(context, close_delay);

    // Good news - tell the people!
    context.channel_message("⭐ ⭐ ⭐ The poll has been opened again for voting. ⭐ ⭐ ⭐", true);

    return true;
  }

  bool reopen(const core::irc::context &context)
  {
    return reopen(context, 0);
  }

  bool close(const core::irc::context &context, uint delay)
  {
    auto &poll = this->get_module().get_poll();

    // If there is no poll in progress, tell the invoking user.
    if (!this->get_module().has_poll()) {
      context.channel_message_to_user("There is no poll taking place right now.");
      return false;
    }

    auto &close_scheduler = this->get_module().get_close_scheduler();
    if (close_scheduler.is_in_progress())
      close_scheduler.cancel();

    // close immediately
    if (delay <= 0) {
      poll.close();
      std::ostringstream message;
      message << "✈ ✈ ✈ Poll is closed now. ✈ ✈ ✈ "
              << poll.vote_count()
              << " votes submitted.";
      context.channel_message(message, true);
      display_results(context);
      return true;
    }

    close_scheduler.close(
      std::chrono::seconds{delay},
      [this, context]
      {
        using namespace std::chrono_literals;

        auto &close_scheduler = this->get_module().get_close_scheduler();
        if (close_scheduler.time_to_expire() > 0s) {
          std::ostringstream message;
          message << "Poll will be closed in "
                  << std::chrono::round<std::chrono::seconds>(close_scheduler.time_to_expire()).count()
                  << " seconds!";
          context.channel_message(message, true);
          return;
        }

        close_now(context);
      }
    );

    return true;
  }

  bool close(const core::irc::context &context)
  {
    return close(context, 60);
  }

  bool close_now(const core::irc::context &context)
  {
    return close(context, 0);
  }

  bool cancel_close(const core::irc::context &context)
  {
    // If there is no poll in progress, tell the invoking user.
    if (!this->get_module().has_poll()) {
      context.channel_message_to_user("There is no poll taking place right now.");
      return false;
    }

    auto &close_scheduler = this->get_module().get_close_scheduler();
    if (!close_scheduler.is_in_progress()) {
      context.channel_message_to_user("This poll is not about to be closed.");
      return false;
    }

    close_scheduler.cancel();
    context.channel_message("Closing of poll canceled.");
    return true;
  }

  bool display_results(const core::irc::context &context)
  {
    // If there is no poll in progress, tell the invoking user.
    if (!this->get_module().has_poll()) {
      context.channel_message_to_user("There is no poll taking place right now.");
      return false;
    }

    auto &poll = this->get_module().get_poll();

    // If the poll is still in progress...
    if (poll.is_open()) {
      context.channel_message_to_user("This poll is still open for guesses!");
      return false;
    }

    // No votes submitted?
    if (poll.vote_count() == 0) {
      context.channel_message("No votes submitted!");
      return true;
    }

    std::ostringstream message;

    const auto &options = poll.get_options();
    const auto results = poll.get_results();

    for (const auto &result : results) {
      if (!message.str().empty())
        message << " | ";
      message << options[result.first]
              << ": "
              << result.second
              << " votes";
    }

    context.channel_message(message, true);
    return true;
  }

  bool show_options(const core::irc::context &context)
  {
    const auto &poll = this->get_module().get_poll();
    // If there is no poll in progress, tell the invoking user.
    if (!this->get_module().has_poll() || !poll.is_open()) {
      context.channel_message_to_user("There is no poll taking place right now.");
      return false;
    }

    const auto &options = poll.get_options();

    std::ostringstream message;
    for (std::size_t key = 0; key < options.size(); ++key) {
      if (key != 0)
        message << " | ";
      message << (key + 1)
              << ". "
              << options[key];
    }
    context.channel_message(message, true);
    return true;
  }
};

}
