//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "message_handler.hpp"
#include "../irc/context.hpp"

namespace core::module
{

template<class Module>
class module;

template<typename Command, typename Module>
class command_message_handler : public message_handler
{
public:
  template<typename... Args>
  explicit command_message_handler(Module &module, Args &&... args)
    : command_{module, std::forward<Args>(args)...}
  { }

  bool enable() override
  {
    return command_.enable();
  }

  bool on_message(const irc::context &context, const irc::protocol::message &message) override
  {
    const auto &chat_line = message.get_param(1);

    if (chat_line.size() < 1 + command_.get_name().size())
      return false;
    if (chat_line[0] != '!')
      return false;

    std::string command = chat_line.substr(1, command_.get_name().size());
    std::transform(command.begin(), command.end(), command.begin(),
      [](unsigned char c){ return std::tolower(c); });
    if (command != command_.get_name())
      return false;

    return command_(context, message);
  }

private:
  Command command_;
};

}
