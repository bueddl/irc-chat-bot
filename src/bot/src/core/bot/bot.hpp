//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "detail/bot_traits.hpp"
#include "../irc/client.hpp"
#include "../module/module_manager.hpp"
#include "../plugin_system/plugin_manager.hpp"
#include "../../modules/builtin/builtin_module.hpp"

#include <boost/bind.hpp>
#include <boost/asio/io_service.hpp>

namespace core::bot
{

using reply_code = core::irc::protocol::reply_code;
using error_code = core::irc::protocol::error_code;
using command = core::irc::protocol::command;

template<typename Traits = detail::bot_traits<>>
class bot
{
public:
  using traits = Traits;
  using socket_type = typename traits::socket_type;
  using connect_strategy = typename traits::connect_strategy;
  using capability_negotiation_strategy = typename traits::capability_negotiation_strategy;
  using client_type = typename traits::client_type;

  static constexpr char version[] = "0.4.4";
  static constexpr char default_plugin_location[] = "/usr/lib/irc-chat-bot/modules";
  static constexpr bool debug_show_statistics = false;

  explicit bot(boost::asio::io_service &io)
    : io_{io},
      client_{io},
      capability_negotiator_{client_},
      module_manager_{io},
      plugin_manager_{module_manager_}
  { }

  void load_default_modules()
  {
    auto default_module = module_manager_.register_module<modules::builtin::builtin_module<bot>>(*this);
    default_module->create();
    default_module->enable();
  }

  void load_plugins(std::filesystem::path plugin_location = default_plugin_location)
  {
    plugin_manager_.load_all(plugin_location);
    plugin_manager_.enable_loaded();
  }

  template<typename CompletionHandler>
  void connect(const std::string &server, uint16_t port,
    CompletionHandler handle_connected,
    const std::string &channel)
  {
    handle_connected_ = handle_connected;

    boost::asio::ip::tcp::resolver resolver{io_};
    boost::asio::ip::tcp::resolver::query query{
      server,
      std::to_string(port)
    };
    auto iterator = resolver.resolve(query);

    client_.connect(iterator,
      boost::bind(&bot::on_connected, this, _1));

    client_.once(reply_code::end_of_motd,
      [channel](auto &client, const irc::protocol::message &message) {
        std::cout << "Logged in." << std::endl;
        client.send(command::join_channel, channel);
      });

    client_.on(command::channel_message,
      [this](auto &client, const auto &message) {
        this->on_channel_message(client, message);

        if (debug_show_statistics)
          std::cout << "Messages received: "
                    << client.statistics().messages_received
                    << ", messages sent: "
                    << client.statistics().messages_sent
                    << std::endl;
      });

    client_.on(reply_code::end_of_names,
      [channel](auto &client, const auto &message) {
        static bool greeted = false;
        if (greeted)
          return;
        greeted = true;

        using namespace std::string_literals;

        client.send(command::channel_message, channel, "/color green");
        client.send(command::channel_message, channel,
          "/me "s
          + "oeo_bot loaded | version "s
          + version
          + " | dev HeyGuys"s);
      });

  }

protected:
  void on_connected(irc::client_base &)
  {
    capability_negotiator_.negotiate(
      "twitch.tv/membership",
      "twitch.tv/tags",
      "twitch.tv/commands");

    if (handle_connected_)
      handle_connected_(client_);
  }

  void on_channel_message(auto &client, const auto &message)
  {
    auto login = message.get_prefix();

    auto display_name_tag = message.get_tag("display-name");
    std::string display_name;
    if (display_name_tag.first)
      display_name = display_name_tag.second;
    else
      display_name = login;

    core::irc::user user{client, login, display_name};

    core::irc::channel channel{client, message.get_param(0)};
    core::irc::context context{channel, user, message};
    module_manager_.on_message(context, message);
  }

private:
  boost::asio::io_service &io_;
  client_type client_;
  capability_negotiation_strategy capability_negotiator_;

  core::module::module_manager module_manager_;
  core::plugin_system::plugin_manager<> plugin_manager_;

  std::function<void(client_type &)> handle_connected_;
};

}
