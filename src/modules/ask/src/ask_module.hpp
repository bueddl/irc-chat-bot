//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "commands/ask_command.hpp"

#include <core/module/module.hpp>

using namespace std::string_view_literals;

namespace modules::ask
{

class ask_module : public core::module::module<ask_module>
{
public:
  using core::module::module<ask_module>::module;

  std::string_view get_name() override
  {
    return "ask"sv;
  }

  std::string_view get_version() override
  {
    return "0.1.2"sv;
  }

  void create() override
  {
    register_command<commands::ask_command>();
  }

  bool is_open() const
  { return is_open_; }

  void is_open(bool set)
  { is_open_ = set; }

private:
  bool is_open_ = true;
};

}
