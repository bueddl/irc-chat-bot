//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "../core/module/authorizer.hpp"
#include "../core/irc/context.hpp"
#include "../core/irc/protocol/message.hpp"

namespace twitch
{

class subsriber_authorizer : public core::module::authorizer
{
public:
  bool is_authorized(const core::irc::context &context, const core::irc::protocol::message &message) override
  {
    auto tag = message.get_tag("subscriber");
    if (!tag.first)
      return false;

    bool is_subscriber = std::stoi(tag.second);
    return is_subscriber;
  }
};

}
