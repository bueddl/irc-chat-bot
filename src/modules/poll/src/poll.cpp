//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "poll.hpp"

namespace modules::poll
{

void poll::open()
{
  open_ = true;
  votes_.clear();
}

void poll::reopen()
{
  open_ = true;
}

void poll::close()
{
  open_ = false;
}

vote_result poll::submit_vote(const core::irc::user &user, uint option_id)
{
  if (!(0 <= option_id && option_id < options_.size()))
    return vote_result::invalid_value;

  if (auto user_vote_iterator = std::find_if(std::begin(votes_), std::end(votes_),
    [&user] (const auto &vote)
    { return vote.get_user() == user; }); user_vote_iterator != std::end(votes_)) {
    // User already voted!
    if (update_allowed_)
      user_vote_iterator->update_value(option_id);
    else
      return vote_result::change_vote_not_allowed;
  }

  votes_.emplace_back(user, option_id);

  return vote_result::success;
}

std::vector<std::pair<uint, std::size_t>> poll::get_results() const
{
  std::vector<std::size_t> accumulated;
  accumulated.resize(options_.size(), 0);

  for (const auto &vote : votes_)
    accumulated[vote.get_option_id()]++;

  std::vector<std::pair<uint, std::size_t>> results;
  for (uint option_id = 0; option_id < options_.size(); ++option_id)
    results.emplace_back(option_id, accumulated[option_id]);

  std::sort(std::begin(results), std::end(results),
    [] (const auto &lhs, const auto &rhs)
    { return lhs.second > rhs.second; });
  return results;
}

}
