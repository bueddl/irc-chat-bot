//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <core/module/command.hpp>
#include <twitch/usertype_authorizer.hpp>

#include <chrono>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

namespace modules::ask::commands
{

template<typename Module>
class ask_command : public core::module::command<ask_command<Module>, Module>
{
  bool use_reply_confirmation = true;
public:
  using core::module::command<ask_command, Module>::command;

  std::string_view get_name() const override
  {
    return getenv("CHATBOT_MOD_ASK_COMMAND");
  }

  bool enable() override
  {
    this->add_syntax_with_capture(
      &ask_command::ask);
    this->add_syntax(
      "enable",
      &ask_command::enable,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<bool>(
      "enable <reply-confirmation>",
      &ask_command::enable_ex,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->add_syntax(
      "disable",
      &ask_command::disable,
      twitch::usertype_authorizer{twitch::usertype::mod});

    this->add_syntax(
      &ask_command::display_help);

    return true;
  }

private:
  bool display_help(const core::irc::context &context)
  {
    if (!this->get_module().is_open()) {
      context.channel_message_to_user("Asking questions is currently disabled.");
      return true;
    }

    context.channel_message("Submit your question by using the ask command: !ask your-question...");
    return true;
  }

  bool enable(const core::irc::context &context)
  {
    return this->enable_ex(context, true);
  }

  bool enable_ex(const core::irc::context &context, bool enable_reply_confirmation)
  {
    if (this->get_module().is_open()) {
      context.channel_message_to_user("Already open!");
      return false;
    }

    this->get_module().is_open(true);
    use_reply_confirmation = enable_reply_confirmation;
    context.channel_message("Ask your questions by using the ask command: !ask your-question...");
    return true;
  }

  bool disable(const core::irc::context &context)
  {
    if (!this->get_module().is_open()) {
      context.channel_message_to_user("Already closed!");
      return false;
    }

    this->get_module().is_open(false);
    context.channel_message("Question time is over :(");
    return true;
  }

  bool ask(const core::irc::context &context, const std::vector<std::string> &args)
  {
    if (!this->get_module().is_open()) {
      context.channel_message_to_user("Sorry, asking questions has not been enabled yet :(");
      return false;
    }

    std::string text;
    for (auto const &word : args) {
      if (!text.empty())
        text += ' ';
      text += word;
    }


    rapidjson::Document d;
    d.SetObject();

    rapidjson::Value message;
    message.SetString(text.c_str(), text.size());
    d.AddMember("message", message, d.GetAllocator());

    rapidjson::Value user;
    user.SetString(context.get_user().display_name().c_str(), context.get_user().display_name().size());
    d.AddMember("user_name", user, d.GetAllocator());

    rapidjson::Value message_id;

    auto [tag_present, tag_id] = context.original_message().get_tag("id");
    if (!tag_present) {
      std::cout << "Message id missing!\n";
      return false;
    }

    message_id.SetString(tag_id.c_str(), tag_id.size());
    d.AddMember("message_id", message_id, d.GetAllocator());

    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    d.Accept(writer);

    std::cout << buffer.GetString() << std::endl;

    try {
      curlpp::Cleanup cleaner;
      curlpp::Easy request;

      request.setOpt(new curlpp::options::Url(getenv("CHATBOT_MOD_ASK_SUBMIT_URL")));
      request.setOpt(new curlpp::options::Verbose(false));

      std::list<std::string> header;
      header.push_back("Content-Type: application/octet-stream");

      request.setOpt(new curlpp::options::HttpHeader(header));

      request.setOpt(new curlpp::options::PostFields(buffer.GetString()));
      request.setOpt(new curlpp::options::PostFieldSize(buffer.GetSize()));

      request.perform();
      std::ostringstream os;
      //os << request;
    }
    catch ( curlpp::LogicError & e ) {
      std::cout << e.what() << std::endl;
    }
    catch ( curlpp::RuntimeError & e ) {
      std::cout << e.what() << std::endl;
    }

   if (use_reply_confirmation)
     context.channel_message_to_user("question queued R)");
   return true;
  }

};

}
