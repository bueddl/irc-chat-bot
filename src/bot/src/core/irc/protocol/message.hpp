//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <string>
#include <optional>
#include <map>
#include <sstream>
#include <vector>
#include "../cistring.hpp"

namespace core::irc::protocol
{

enum class reply_code
{
  invalid = 0,
  welcome = 1,
  your_host = 2,
  created = 3,
  my_info = 4,
  bounce = 5,
  trace_link = 200,
  trace_connecting = 201,
  trace_handshake = 202,
  trace_unknown = 203,
  trace_operator = 204,
  trace_user = 205,
  trace_server = 206,
  trace_service = 207,
  trace_newtype = 208,
  trace_class = 209,
  trace_reconnect = 210,
  stats_link_info = 211,
  stats_commands = 212,
  end_of_stats = 219,
  umode_is = 221,
  serv_list = 234,
  serv_list_end = 235,
  stats_uptime = 242,
  stats_oline = 243,
  luser_client = 251,
  luser_op = 252,
  luser_unknown = 253,
  luser_channels = 254,
  luser_me = 255,
  admin_me = 256,
  admin_loc1 = 257,
  admin_loc2 = 258,
  admin_email = 259,
  trace_log = 261,
  trace_end = 262,
  try_again = 263,
  away = 301,
  user_host = 302,
  is_on = 303,
  unaway = 305,
  nowaway = 306,
  who_is_user = 311,
  who_is_server = 312,
  who_is_operator = 313,
  who_was_user = 314,
  end_of_who = 315,
  who_is_idle = 317,
  end_of_whois = 318,
  whois_channels = 319,
  list_start = 321,
  list = 322,
  list_end = 323,
  channel_mode_is = 324,
  uniq_op_is = 325,
  no_topic = 331,
  topic = 332,
  inviting = 341,
  summoning = 342,
  invite_list = 346,
  end_of_invite_list = 347,
  except_list = 348,
  end_of_except_list = 349,
  version = 351,
  who_reply = 352,
  nam_reply = 353,
  links = 364,
  end_of_links = 365,
  end_of_names = 366,
  ban_list = 367,
  end_of_ban_list = 368,
  end_of_who_was = 369,
  info = 371,
  motd = 372,
  end_of_info = 374,
  motd_start = 375,
  end_of_motd = 376,
  youre_oper = 381,
  rehashing = 382,
  youre_service = 383,
  time = 391,
  users_start = 392,
  users = 393,
  end_of_users = 394,
  no_users = 395,
};

inline reply_code make_reply_code(int value)
{
  if (value < 1
      || value > 5 && value < 200
      || value > 212 && value < 219
      || value == 220
      || value > 221 && value < 234
      || value > 235 && value < 242
      || value > 243 && value < 251
      || value == 260
      || value > 263 && value < 301
      || value == 304
      || value > 306 && value < 311
      || value == 316
      || value == 320
      || value > 325 && value < 331
      || value > 332 && value < 342
      || value > 342 && value < 346
      || value == 350
      || value > 353 && value < 364
      || value == 370
      || value == 373
      || value > 376 && value < 381
      || value > 383 && value < 391
      || value > 395)
    return reply_code::invalid;

  return static_cast<reply_code>(value);
}

inline std::string to_string(reply_code code)
{
  switch (code) {
  case reply_code::invalid:
    return "invalid";
  case reply_code::welcome:
    return "welcome";
  case reply_code::your_host:
    return "your_host";
  case reply_code::created:
    return "created";
  case reply_code::my_info:
    return "my_info";
  case reply_code::bounce:
    return "bounce";
  case reply_code::trace_link:
    return "trace_link";
  case reply_code::trace_connecting:
    return "trace_connecting";
  case reply_code::trace_handshake:
    return "trace_handshake";
  case reply_code::trace_unknown:
    return "trace_unknown";
  case reply_code::trace_operator:
    return "trace_operator";
  case reply_code::trace_user:
    return "trace_user";
  case reply_code::trace_server:
    return "trace_server";
  case reply_code::trace_service:
    return "trace_service";
  case reply_code::trace_newtype:
    return "trace_newtype";
  case reply_code::trace_class:
    return "trace_class";
  case reply_code::trace_reconnect:
    return "trace_reconnect";
  case reply_code::stats_link_info:
    return "stats_link_info";
  case reply_code::stats_commands:
    return "stats_commands";
  case reply_code::end_of_stats:
    return "end_of_stats";
  case reply_code::umode_is:
    return "umode_is";
  case reply_code::serv_list:
    return "serv_list";
  case reply_code::serv_list_end:
    return "serv_list_end";
  case reply_code::stats_uptime:
    return "stats_uptime";
  case reply_code::stats_oline:
    return "stats_oline";
  case reply_code::luser_client:
    return "luser_client";
  case reply_code::luser_op:
    return "luser_op";
  case reply_code::luser_unknown:
    return "luser_unknown";
  case reply_code::luser_channels:
    return "luser_channels";
  case reply_code::luser_me:
    return "luser_me";
  case reply_code::admin_me:
    return "admin_me";
  case reply_code::admin_loc1:
    return "admin_loc1";
  case reply_code::admin_loc2:
    return "admin_loc2";
  case reply_code::admin_email:
    return "admin_email";
  case reply_code::trace_log:
    return "trace_log";
  case reply_code::trace_end:
    return "trace_end";
  case reply_code::try_again:
    return "try_again";
  case reply_code::away:
    return "away";
  case reply_code::user_host:
    return "user_host";
  case reply_code::is_on:
    return "is_on";
  case reply_code::unaway:
    return "unaway";
  case reply_code::nowaway:
    return "nowaway";
  case reply_code::who_is_user:
    return "who_is_user";
  case reply_code::who_is_server:
    return "who_is_server";
  case reply_code::who_is_operator:
    return "who_is_operator";
  case reply_code::who_was_user:
    return "who_was_user";
  case reply_code::end_of_who:
    return "end_of_who";
  case reply_code::who_is_idle:
    return "who_is_idle";
  case reply_code::end_of_whois:
    return "end_of_whois";
  case reply_code::whois_channels:
    return "whois_channels";
  case reply_code::list_start:
    return "list_start";
  case reply_code::list:
    return "list";
  case reply_code::list_end:
    return "list_end";
  case reply_code::channel_mode_is:
    return "channel_mode_is";
  case reply_code::uniq_op_is:
    return "uniq_op_is";
  case reply_code::no_topic:
    return "no_topic";
  case reply_code::topic:
    return "topic";
  case reply_code::inviting:
    return "inviting";
  case reply_code::summoning:
    return "summoning";
  case reply_code::invite_list:
    return "invite_list";
  case reply_code::end_of_invite_list:
    return "end_of_invite_list";
  case reply_code::except_list:
    return "except_list";
  case reply_code::end_of_except_list:
    return "end_of_except_list";
  case reply_code::version:
    return "version";
  case reply_code::who_reply:
    return "who_reply";
  case reply_code::nam_reply:
    return "nam_reply";
  case reply_code::links:
    return "links";
  case reply_code::end_of_links:
    return "end_of_links";
  case reply_code::end_of_names:
    return "end_of_names";
  case reply_code::ban_list:
    return "ban_list";
  case reply_code::end_of_ban_list:
    return "end_of_ban_list";
  case reply_code::end_of_who_was:
    return "end_of_who_was";
  case reply_code::info:
    return "info";
  case reply_code::motd:
    return "motd";
  case reply_code::end_of_info:
    return "end_of_info";
  case reply_code::motd_start:
    return "motd_start";
  case reply_code::end_of_motd:
    return "end_of_motd";
  case reply_code::youre_oper:
    return "youre_oper";
  case reply_code::rehashing:
    return "rehashing";
  case reply_code::youre_service:
    return "youre_service";
  case reply_code::time:
    return "time";
  case reply_code::users_start:
    return "users_start";
  case reply_code::users:
    return "users";
  case reply_code::end_of_users:
    return "end_of_users";
  case reply_code::no_users:
    return "no_users";
  }
}

enum class error_code
{
  invalid = 0,
  no_such_nick = 401,
  no_such_server = 402,
  no_such_channel = 403,
  cannot_send_to_chan = 404,
  too_many_channels = 405,
  was_no_such_nick = 406,
  too_many_targets = 407,
  no_such_service = 408,
  no_origin = 409,
  no_recipient = 411,
  no_text_to_send = 412,
  no_toplevel = 413,
  wild_toplevel = 414,
  bad_mask = 415,
  unknown_command = 421,
  no_motd = 422,
  no_admin_info = 423,
  file_error = 424,
  no_nickname_given = 431,
  erroneus_nickname = 432,
  nickname_in_use = 433,
  nick_collision = 436,
  unavail_resource = 437,
  user_not_in_channel = 441,
  not_on_channel = 442,
  user_on_channel = 443,
  no_login = 444,
  summon_disabled = 445,
  users_disabled = 446,
  not_registered = 451,
  need_more_params = 461,
  already_registred = 462,
  no_perm_for_host = 463,
  passwd_mismatch = 464,
  youre_banned_creep = 465,
  you_will_be_banned = 466,
  key_set = 467,
  channel_is_full = 471,
  unknown_mode = 472,
  invite_only_chan = 473,
  banned_from_chan = 474,
  bad_channel_key = 475,
  bad_chan_mask = 476,
  no_chan_modes = 477,
  ban_list_full = 478,
  no_privileges = 481,
  chan_oprivs_needed = 482,
  cant_kill_server = 483,
  restricted = 484,
  uniq_op_privs_needed = 485,
  no_oper_host = 491,
  umode_unknown_flag = 501,
  users_dont_match = 502,
};

inline error_code make_error_code(int value)
{
  if (value < 401
      || value == 410
      || value > 415 && value < 421
      || value > 424 && value < 431
      || value > 433 && value < 436
      || value > 437 && value < 441
      || value > 446 && value < 451
      || value > 451 && value < 461
      || value > 467 && value < 471
      || value > 478 && value < 481
      || value > 485 && value < 491
      || value > 491 && value < 501
      || value > 502)
    return error_code::invalid;

  return static_cast<error_code>(value);
}

inline std::string to_string(error_code code)
{
  switch (code) {
  case error_code::invalid:
    return "invalid";
  case error_code::no_such_nick:
    return "no_such_nick";
  case error_code::no_such_server:
    return "no_such_server";
  case error_code::no_such_channel:
    return "no_such_channel";
  case error_code::cannot_send_to_chan:
    return "cannot_send_to_chan";
  case error_code::too_many_channels:
    return "too_many_channels";
  case error_code::was_no_such_nick:
    return "was_no_such_nick";
  case error_code::too_many_targets:
    return "too_many_targets";
  case error_code::no_such_service:
    return "no_such_service";
  case error_code::no_origin:
    return "no_origin";
  case error_code::no_recipient:
    return "no_recipient";
  case error_code::no_text_to_send:
    return "no_text_to_send";
  case error_code::no_toplevel:
    return "no_toplevel";
  case error_code::wild_toplevel:
    return "wild_toplevel";
  case error_code::bad_mask:
    return "bad_mask";
  case error_code::unknown_command:
    return "unknown_command";
  case error_code::no_motd:
    return "no_motd";
  case error_code::no_admin_info:
    return "no_admin_info";
  case error_code::file_error:
    return "file_error";
  case error_code::no_nickname_given:
    return "no_nickname_given";
  case error_code::erroneus_nickname:
    return "erroneus_nickname";
  case error_code::nickname_in_use:
    return "nickname_in_use";
  case error_code::nick_collision:
    return "nick_collision";
  case error_code::unavail_resource:
    return "unavail_resource";
  case error_code::user_not_in_channel:
    return "user_not_in_channel";
  case error_code::not_on_channel:
    return "not_on_channel";
  case error_code::user_on_channel:
    return "user_on_channel";
  case error_code::no_login:
    return "no_login";
  case error_code::summon_disabled:
    return "summon_disabled";
  case error_code::users_disabled:
    return "users_disabled";
  case error_code::not_registered:
    return "not_registered";
  case error_code::need_more_params:
    return "need_more_params";
  case error_code::already_registred:
    return "already_registred";
  case error_code::no_perm_for_host:
    return "no_perm_for_host";
  case error_code::passwd_mismatch:
    return "passwd_mismatch";
  case error_code::youre_banned_creep:
    return "youre_banned_creep";
  case error_code::you_will_be_banned:
    return "you_will_be_banned";
  case error_code::key_set:
    return "key_set";
  case error_code::channel_is_full:
    return "channel_is_full";
  case error_code::unknown_mode:
    return "unknown_mode";
  case error_code::invite_only_chan:
    return "invite_only_chan";
  case error_code::banned_from_chan:
    return "banned_from_chan";
  case error_code::bad_channel_key:
    return "bad_channel_key";
  case error_code::bad_chan_mask:
    return "bad_chan_mask";
  case error_code::no_chan_modes:
    return "no_chan_modes";
  case error_code::ban_list_full:
    return "ban_list_full";
  case error_code::no_privileges:
    return "no_privileges";
  case error_code::chan_oprivs_needed:
    return "chan_oprivs_needed";
  case error_code::cant_kill_server:
    return "cant_kill_server";
  case error_code::restricted:
    return "restricted";
  case error_code::uniq_op_privs_needed:
    return "uniq_op_privs_needed";
  case error_code::no_oper_host:
    return "no_oper_host";
  case error_code::umode_unknown_flag:
    return "umode_unknown_flag";
  case error_code::users_dont_match:
    return "users_dont_match";
  }
}

enum class command
{
  invalid = 0,

  // connection
    set_connection_password = 1001,
  set_connection_nickname,
  set_connection_user,
  connection_quit,
  server_connection_quit,
  ping,
  pong,
  capability,

  obtain_operator_privileges,
  change_mode,
  register_service,

  // channel ops
    join_channel,
  leave_channel,
  set_channel_topic,
  list_names,
  list_channels,
  invite_user,
  kick_user,
  channel_message,
  channel_notice,

  // server
    get_motd,
  get_version,
};

inline command make_command(const cistring &value)
{
  if (value == "pass")
    return command::set_connection_password;
  if (value == "nick")
    return command::set_connection_nickname;
  if (value == "user")
    return command::set_connection_user;
  if (value == "quit")
    return command::connection_quit;
  if (value == "squit")
    return command::server_connection_quit;
  if (value == "ping")
    return command::ping;
  if (value == "pong")
    return command::pong;
  if (value == "cap")
    return command::capability;

  if (value == "oper")
    return command::obtain_operator_privileges;
  if (value == "mode")
    return command::change_mode;
  if (value == "service")
    return command::register_service;

  if (value == "join")
    return command::join_channel;
  if (value == "part")
    return command::leave_channel;
  if (value == "topic")
    return command::set_channel_topic;
  if (value == "names")
    return command::list_names;
  if (value == "list")
    return command::list_channels;
  if (value == "invite")
    return command::invite_user;
  if (value == "kick")
    return command::kick_user;
  if (value == "privmsg")
    return command::channel_message;
  if (value == "notice")
    return command::channel_notice;

  if (value == "motd")
    return command::get_motd;
  if (value == "version")
    return command::get_version;

  return command::invalid;
}

inline std::string to_string(command value)
{
  switch (value) {
  case command::set_connection_password:return "pass";
  case command::set_connection_nickname:return "nick";
  case command::set_connection_user:return "user";
  case command::connection_quit:return "quit";
  case command::server_connection_quit:return "squit";
  case command::ping:return "ping";
  case command::pong:return "pong";
  case command::capability:return "cap";
  case command::obtain_operator_privileges:return "oper";
  case command::change_mode:return "mode";
  case command::register_service:return "service";
  case command::join_channel:return "join";
  case command::leave_channel:return "part";
  case command::set_channel_topic:return "topic";
  case command::list_names:return "names";
  case command::list_channels:return "list";
  case command::invite_user:return "invite";
  case command::kick_user:return "kick";
  case command::channel_message:return "privmsg";
  case command::channel_notice:return "notice";
  case command::get_motd:return "motd";
  case command::get_version:return "version";

  default:return "";
  }
}

template<typename InputIterator, typename OutputIterator>
inline bool escape_tag(InputIterator begin, InputIterator end, OutputIterator dest)
{
  bool neededEscape = false;

  for (InputIterator iterator = begin; iterator != end; ++iterator) {
    switch (auto ch = *iterator; ch) {
    case ';':neededEscape = true;
      *dest++ = '\\';
      *dest++ = ':';
      break;

    case ' ':neededEscape = true;
      *dest++ = '\\';
      *dest++ = 's';
      break;

    case '\\':neededEscape = true;
      *dest++ = '\\';
      *dest++ = '\\';
      break;

    case '\r':neededEscape = true;
      *dest++ = '\\';
      *dest++ = 'r';
      break;

    case '\n':neededEscape = true;
      *dest++ = '\\';
      *dest++ = 'n';
      break;

    default:*dest++ = *iterator;
    }
  }

  return neededEscape;
}

class message
{
public:
  std::string &get_prefix()
  { return prefix_; }

  const std::string &get_prefix() const
  { return prefix_; }

  void set_prefix(const std::string &_prefix)
  {
    prefix_ = _prefix;
  }

  auto &get_tags()
  { return tags_; }

  const auto &get_tags() const
  { return tags_; }

  void set_tags(std::map<std::string, std::optional<std::string>> &&tags)
  {
    tags_ = tags;
  }

  std::pair<bool, std::string> get_tag(const std::string &key) const
  {
    if (const auto &tag = tags_.find(key); tag != tags_.end())
      return {tag->second.has_value(), tag->second.value_or("")};

    return {false, ""};
  };

  auto &get_params()
  { return params_; }

  const auto &get_params() const
  { return params_; }

  void set_params(std::vector<std::string> &&params)
  {
    params_ = params;
  }

  const std::string &get_param(std::size_t index) const
  { return params_[index]; }

  virtual std::string str() const
  {
    std::stringstream stream;

    serialize_tags_(stream);
    serialize_prefix_(stream);
    serialize_command_(stream);
    serialize_params_(stream);

    stream << "\r\n";
    return stream.str();
  }

  bool is_command() const
  { return code_ > 1000; }

  command get_command() const
  { return static_cast<command>(code_); }

  bool is_reply() const
  { return code_ > 0 && code_ < 400; }

  reply_code get_reply_code() const
  { return static_cast<reply_code>(code_); }

  bool is_error() const
  { return code_ >= 400 && code_ < 600; }

  error_code get_error_code() const
  { return static_cast<error_code>(code_); }

  int32_t get_code() const
  { return code_; }

  void set_code(int code)
  {
    code_ = code;
  }

protected:
  virtual void serialize_tags_(std::stringstream &stream) const
  {
    if (!tags_.empty()) {
      stream << '@';
      for (auto iterator = tags_.begin(); iterator != tags_.end(); ++iterator) {
        const auto &tag = *iterator;
        stream << tag.first;
        if (tag.second) {
          std::string value;
          escape_tag(tag.second.value().begin(), tag.second.value().end(), std::back_inserter(value));
          stream << '=' << value;
        }
        if (iterator != std::prev(tags_.end()))
          stream << ';';
      }
      stream << ' ';
    }
  }

  virtual void serialize_prefix_(std::stringstream &stream) const
  {
    if (!prefix_.empty())
      stream << ':' << prefix_ << ' ';
  }

  virtual void serialize_command_(std::stringstream &stream) const
  {
    if (is_command())
      stream << to_string(get_command());

    if (is_reply() || is_error())
      stream << code_;
  }

  virtual void serialize_params_(std::stringstream &stream) const
  {
    for (auto iterator = params_.begin(); iterator != params_.end(); ++iterator) {
      const auto &param = *iterator;

      if (iterator == std::prev(params_.end())) {
        if (param.find(' ') != std::string::npos) {
          stream << " :" << param;
          break;
        }
      }

      stream << ' ' << param;
    }
  }

private:
  std::string prefix_;
  std::map<std::string, std::optional<std::string>> tags_;
  std::vector<std::string> params_;

  int32_t code_ = 0;
};

};
