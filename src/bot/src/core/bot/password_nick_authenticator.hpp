//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

namespace core::bot
{

class password_nick_authenticator
{
public:
  password_nick_authenticator(std::string password, std::string nickname)
    : password_{std::move(password)},
      nickname_{std::move(nickname)}
  { }

  template<typename Client>
  void operator()(Client &client)
  {
    client.send(command::set_connection_password, password_);
    client.send(command::set_connection_nickname, nickname_);
  }

private:
  std::string password_;
  std::string nickname_;
};

}
