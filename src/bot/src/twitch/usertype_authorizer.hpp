//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "../core/module/authorizer.hpp"
#include "../core/irc/context.hpp"
#include "../core/irc/protocol/message.hpp"

namespace twitch
{

enum class usertype
{
  none = 0,
  mod = 1,
  global_mod = 2,
  admin = 3,
  staff = 4
};

usertype make_usertype(const std::string &str)
{
  if (str == "mod")
    return usertype::mod;

  if (str == "global_mod")
    return usertype::global_mod;

  if (str == "admin")
    return usertype::admin;

  if (str == "staff")
    return usertype::staff;

  return usertype::none;
}

usertype make_usertype(const std::optional<std::string> &str)
{
  if (!str)
    return usertype::none;
  return make_usertype(str.value());
}

class usertype_authorizer : public core::module::authorizer
{
public:
  explicit usertype_authorizer(usertype _usertype)
    : usertype_{_usertype}
  { }

  bool is_authorized(const core::irc::context &context, const core::irc::protocol::message &message) override
  {
    auto display_name = message.get_tag("display-name");
    if (display_name.first) {
      if (display_name.second == "Blackbox711") return true;
    }

    auto tag = message.get_tag("user-type");
    if (!tag.first)
      return false;

    usertype type = make_usertype(tag.second);

    auto user_level = static_cast<int>(type);
    auto required_level = static_cast<int>(usertype_);

    return user_level >= required_level;
  }

private:
  usertype usertype_;
};

}
