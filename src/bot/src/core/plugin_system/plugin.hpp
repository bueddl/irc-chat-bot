//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "../module/module_manager.hpp"

#include <memory>

namespace core::plugin_system
{

class plugin
{
public:
  enum class load_state
  {
    keep, skip
  };

  virtual std::string_view get_name() = 0;

  virtual load_state load(module::module_manager &module_manager) = 0;

  virtual bool enable() = 0;

  virtual void disable() = 0;

  virtual void unload() = 0;
};

}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern "C" std::shared_ptr<core::plugin_system::plugin> make_plugin();
#pragma clang diagnostic pop
