//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "message_handler.hpp"
#include "command_message_handler.hpp"
#include "../irc/context.hpp"
#include "../irc/protocol/message.hpp"

#include <memory>
#include <forward_list>

namespace core::module
{

class module_base
{
public:
  virtual std::string_view get_name() = 0;

  virtual std::string_view get_version() = 0;

  virtual void create()
  { }

  virtual bool enable()
  {
    return true;
  }

  virtual void disable()
  {
    // NO-OP for now
  }

  virtual bool on_message(const irc::context &context, const irc::protocol::message &message)
  {
    return false;
  }

  virtual void destroy()
  { }
};

template<typename Module>
class module : public module_base
{
public:
  using module_concrete_type = Module;

  explicit module(boost::asio::io_service &io)
    : io_{io}
  { }

  bool enable() override
  {
    for (auto &message_handler : message_handlers_)
      if (!message_handler->enable())
        return false;

    return true;
  }

  bool on_message(const irc::context &context, const irc::protocol::message &message) override
  {
    for (auto &message_handler : message_handlers_)
      if (message_handler->on_message(context, message))
        return true;

    return false;
  }

  boost::asio::io_service &get_service()
  { return io_; }

protected:
  template<typename T, typename... As>
  void register_command(As &&... args)
  {
    using command_message_handler_type = command_message_handler<T, module_concrete_type>;

    register_message_handler<command_message_handler_type>(
      reinterpret_cast<module_concrete_type &>(*this),
      std::forward<As>(args)...);
  }

  template<template<typename> typename T, typename... As>
  void register_command(As &&... args)
  {
    using command_message_handler_type = command_message_handler<T<module_concrete_type>, module_concrete_type>;

    register_message_handler<command_message_handler_type>(
      reinterpret_cast<module_concrete_type &>(*this),
      std::forward<As>(args)...);
  }

  template<typename T, typename... As>
  void register_message_handler(As &&... args)
  {
    auto handler = std::make_unique<T>(std::forward<As>(args)...);
    message_handlers_.emplace_front(std::move(handler));
  }

private:
  std::forward_list<std::unique_ptr<message_handler>> message_handlers_;
  boost::asio::io_service &io_;
};


}
