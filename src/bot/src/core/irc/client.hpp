//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "cistring.hpp"
#include "protocol/message.hpp"
#include "protocol/message_parser.hpp"

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <functional>
#include <iostream>
#include <list>

#include <cstdint>
#include <experimental/iterator>

namespace core::irc
{

template<std::size_t N>
inline std::string to_string(const char (&cstr)[N])
{
  return cstr;
}

inline std::string to_string(const std::string &str)
{
  return str;
}

class client_base
{
public:
  template<typename Signature>
  class event_handler;

  template<typename Result, typename... Args>
  class event_handler<Result(Args...)>
  {
  public:
    using signature = Result(Args...);
    using function_type = std::function<signature>;

    explicit event_handler(function_type function, bool apply_once = false)
      : function_{function},
        apply_once_(apply_once)
    { }

    bool apply_once() const
    { return apply_once_; }

    Result invoke(Args &&... args)
    {
      return function_(std::forward<Args>(args)...);
    }

  private:
    function_type function_;
    bool apply_once_;
  };

  struct connection_statistics
  {
    std::size_t messages_sent = 0;
    std::size_t messages_received = 0;
  };

  using event_handler_signature = void(client_base &, const irc::protocol::message &message);
  using event_handler_function = std::function<event_handler_signature>;

  using event_handler_type = event_handler<event_handler_signature>;

  template<typename Code, typename Handler>
  void on(Code code, Handler handler, bool once = false)
  {
    auto key = static_cast<int>(code);
    if (auto it = handlers_.find(key); it == handlers_.end())
      handlers_.emplace(key, std::list<event_handler_type>{ });

    handlers_[key].emplace_front(handler, once);
  }

  template<typename Code, typename Handler>
  void once(Code code, Handler &&handler)
  {
    on(code, std::forward<Handler>(handler), true);
  }

  template<typename... Args>
  void send(protocol::command command, Args &&... args)
  {
    irc::protocol::message message;
    message.set_code(static_cast<int32_t>(command));

    if (sizeof...(Args) > 0)
      setup_message_(message, std::forward<Args>(args)...);

    debug_message_(message, false);
    send_message(message);

    ++statistics_.messages_sent;
  }

  const connection_statistics &statistics() const
  { return statistics_; }

protected:
  virtual void send_message(const irc::protocol::message &message) = 0;

  void on_message(const irc::protocol::message &message)
  {
    ++statistics_.messages_received;

    const auto &code = message.get_code();

    debug_message_(message, true);

    if (auto iter = handlers_.find(code); iter != handlers_.end()) {
      for (auto event_handler_iterator = std::begin(iter->second); event_handler_iterator != std::end(iter->second);) {
        auto &ev = *event_handler_iterator;

        ev.invoke(*this, message);

        if (ev.apply_once())
          event_handler_iterator = iter->second.erase(event_handler_iterator);
        else
          ++event_handler_iterator;
      }
    }
  }

private:
  std::map<int, std::list<event_handler_type>> handlers_;

  connection_statistics statistics_;

  template<typename Arg, typename... Args>
  void setup_message_(irc::protocol::message &message, Arg &&arg, Args &&... args)
  {
    using std::to_string;
    using core::irc::to_string;

    message.get_params().push_back(to_string(arg));

    if constexpr (sizeof...(args))
      setup_message_(message, std::forward<Args>(args)...);
  }

  void debug_message_(const irc::protocol::message &message, bool received) const
  {
    /// DEBUG

    if (received)
      std::cerr << ">>> ";
    else
      std::cerr << "<<< ";

    const auto &code = message.get_code();
    if (message.is_command())
      std::cerr << to_string(static_cast<protocol::command>(code));
    else if (message.is_reply())
      std::cerr << to_string(static_cast<protocol::reply_code>(code));
    else
      std::cerr << to_string(static_cast<protocol::error_code>(code));
    std::cerr << ": tags = { ";
    bool first = true;
    for (auto const &[key, value] : message.get_tags()) {
      if (!first)
        std::cerr << ", ";
      std::cerr << '"' << key << '"';
      if (value.has_value())
        std::cerr << " = \"" << value.value() << '"';
      first = false;
    }
    std::cerr << " }";
    std::cerr << " params = [ ";
    first = true;
    for (auto const &param : message.get_params()) {
      if (!first)
        std::cerr << ", ";
      std::cerr << '"' << param << '"';
      first = false;
    }
    std::cerr << " ]";

    std::cerr << "\n";

    /// END DEBUG
  }
};

template<typename Socket, typename ConnectStrategy>
class client : public client_base
{
public:
  using socket_type = Socket;
  using connect_strategy = ConnectStrategy;

  using event_handler_type = client_base::event_handler_type;

  explicit client(boost::asio::io_service &context)
    : connect_strategy_{context},
      socket_{connect_strategy_.socket()}
  {
    on(protocol::command::ping, [](auto &client, const auto &message) {
      client.send(protocol::command::pong, message.get_params()[0]);
    });
  }

  template<typename CompletionHandler>
  void connect(boost::asio::ip::tcp::resolver::iterator endpoint_iterator, CompletionHandler handle_connect)
  {
    handle_connect_ = handle_connect;

    connect_strategy_.connect(endpoint_iterator,
      boost::bind(&client::handle_connect, this,
        boost::asio::placeholders::error));
  }

protected:
  void send_message(const irc::protocol::message &message) override
  {
    auto data = message.str();

    boost::asio::async_write(socket_, boost::asio::buffer(data, data.size()),
      boost::bind(&client::handle_write, this,
        boost::asio::placeholders::error,
        boost::asio::placeholders::bytes_transferred
      ));
  }

private:
  connect_strategy connect_strategy_;
  socket_type &socket_;

  boost::asio::streambuf read_buffer_;
  std::function<void(client &)> handle_connect_;

  void handle_connect(const boost::system::error_code &ec)
  {
    if (ec)
      throw ec;

    boost::asio::async_read_until(socket_, read_buffer_, "\r\n",
      boost::bind(&client::handle_read, this,
        boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

    handle_connect_(*this);
  }

  void handle_read(const boost::system::error_code &ec, std::size_t bytes_transferred)
  {
    if (ec) {
      std::cerr << "[read error] " << ec << " (" << ec.message() << ")" << std::endl;
      return;
    }

    std::string line(
      boost::asio::buffers_begin(read_buffer_.data()),
      boost::asio::buffers_begin(read_buffer_.data()) + bytes_transferred);
    read_buffer_.consume(bytes_transferred);

    irc::protocol::message_parser message_parser{line};

    irc::protocol::message message;
    if (!message_parser.parse(message)) {
      std::cerr << "message parsing error\n" << "\tmessage: " << line << "\n";

    }

    on_message(message);

    boost::asio::async_read_until(socket_, read_buffer_, "\r\n",
      boost::bind(&client::handle_read, this,
        boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
  }

  void handle_write(const boost::system::error_code &ec, std::size_t bytes_transferred)
  {
    if (ec) {
      std::cerr << "[write error] " << ec << " (" << ec.message() << ")" << std::endl;
      return;
    }
  }
};

}
