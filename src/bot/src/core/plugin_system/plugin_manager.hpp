//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "plugin_loader.hpp"
#include "../module/module_manager.hpp"

#include <vector>
#include <memory>

#ifdef __cpp_lib_filesystem
#include <filesystem>
#else
#include <experimental/filesystem>
namespace std
{
namespace filesystem = std::experimental::filesystem;
}
#endif

#include <sstream>

using namespace std::string_literals;

namespace core::plugin_system
{

template<typename T = plugin_loader<>>
class plugin_manager
{
public:
  using plugin_loader_type = T;
  using plugin_storage_type = std::shared_ptr<plugin>;
  using make_plugin_fn_type = plugin_storage_type();

  plugin_manager(const plugin_manager &) = default;

  plugin_manager(plugin_manager &&) noexcept = default;

  explicit plugin_manager(module::module_manager &module_manager)
    : m_module_manager{module_manager}
  { }

  virtual ~plugin_manager()
  {
    return;
    for (auto &plugin_wrapper : m_plugin_wrappers) {
      plugin_wrapper.plugin->unload();
      plugin_wrapper.plugin.reset();
    }
  }

  void load(const std::filesystem::path &path)
  {
    plugin_loader_type loader;
    loader.open(path.string());
    if (!loader.is_open()) {
      std::cerr << "invalid plugin: " << path << ". Error loading shared object file!" << std::endl;
      return;
    }

    auto make_plugin = loader.template resolve_symbol<make_plugin_fn_type>("make_plugin");
    if (make_plugin == nullptr) {
      std::cerr << "invalid plugin: " << path << ". make_plugin() function not found!" << std::endl;
      return;
    }

    auto plugin = make_plugin();

    if (plugin->load(m_module_manager) == plugin::load_state::skip)
      return;

    m_plugin_wrappers.emplace_back(_plugin_wrapper{
      std::move(loader),
      plugin
    });
  }

  void load_all(const std::filesystem::path &path)
  {
    for (auto &p : std::filesystem::directory_iterator{path})
      load(p);
  }

  void enable_loaded()
  {
    for (auto &plugin_wrapper : m_plugin_wrappers) {
      if (plugin_enabled(plugin_wrapper.plugin->get_name())) {

        std::cout << "Module enabled: "
          << plugin_wrapper.plugin->get_name()
          << std::endl;

        plugin_wrapper.plugin->enable();
      }
    }
  }

private:
  struct _plugin_wrapper
  {
    plugin_loader_type loader;
    plugin_storage_type plugin;
  };
  std::vector<_plugin_wrapper> m_plugin_wrappers;

  module::module_manager &m_module_manager;

  bool plugin_enabled(std::string_view needle) const
  {
    std::stringstream ss(::getenv("CHATBOT_MODULES_ENABLED"));

    while(ss.good())
    {
      std::string substr;
      getline(ss, substr, ',');
      if (substr == needle)
        return true;
    }

    return false;
  }
};

}
