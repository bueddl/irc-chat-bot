//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "command_base.hpp"
#include "command_parser.hpp"
#include "args.hpp"
#include "authorizer.hpp"

#include <memory>
#include <forward_list>

using namespace std::string_literals;

namespace core::module
{

template<typename T, typename Module>
class command : public command_base
{
  using module_type = Module;

  template<typename Authorizer, bool, typename... As>
  class command_syntax;

public:
  explicit command(Module &mod)
    : module_{mod}
  { }

  bool operator()(const irc::context &context, const irc::protocol::message &message) override
  {
    const std::string chat_line = message.get_param(1)
      .substr(1 + this->get_name().size());

    for (auto &syntax : available_syntaxes_) {
      if (auto args = syntax->parse(chat_line); args->is_valid())
        return args->invoke(context, message);
    }
    return false;
  }

protected:
  template<typename... As, typename Authorizer = void_authorizer>
  void add_syntax(
    const std::string &syntax_expr,
    handler_function_t<false, T, As...> function,
    Authorizer &&authorizer = Authorizer{ })
  {
    add_syntax_impl<false, As...>(
      syntax_expr,
      function,
      std::forward<Authorizer>(authorizer));
  }

  template<typename Authorizer = void_authorizer>
  void add_syntax(
    handler_function_t<false, T> function,
    Authorizer &&authorizer = Authorizer{ })
  {
    add_syntax(""s, function, std::move(authorizer));
  }

  template<typename... As, typename Authorizer = void_authorizer>
  void add_syntax_with_capture(
    const std::string &syntax_expr,
    handler_function_t<true, T, As...> function,
    Authorizer &&authorizer = Authorizer{ })
  {
    add_syntax_impl<true, As...>(
      syntax_expr,
      function,
      std::forward<Authorizer>(authorizer));
  }

  template<typename Authorizer = void_authorizer>
  void add_syntax_with_capture(
    handler_function_t<true, T> function,
    Authorizer &&authorizer = Authorizer{ })
  {
    add_syntax_with_capture(""s, function, std::move(authorizer));
  }

  module_type &get_module()
  { return module_; }

  const module_type &get_module() const
  { return module_; }

private:
  class command_invoker_base
  {
  public:
    virtual ~command_invoker_base() = default;

    virtual bool is_valid() const = 0;

    virtual bool invoke(const irc::context &context, const irc::protocol::message &message) = 0;
  };

  template<typename Syntax, typename... As>
  class command_invoker : public command_invoker_base
  {
  public:
    command_invoker(Syntax &syntax, args<As...> &&args)
      : syntax_{syntax},
        args_{std::move(args)}
    { }

    bool is_valid() const override
    {
      return args_.is_valid();
    }

    bool invoke(const irc::context &context, const irc::protocol::message &message) override
    {
      return syntax_(context, message, args_);
    }

  private:
    args<As...> args_;
    Syntax &syntax_;
  };

  class syntax_base
  {
  public:
    virtual ~syntax_base() = default;

    virtual std::unique_ptr<command_invoker_base> parse(const std::string &chatLine) = 0;
  };

  template<bool, typename Authorizer, typename... As>
  struct command_syntax_traits
  {
    using syntax_type = command_syntax<Authorizer, false, As...>;

    using args_type = args<As...>;

    using command_parser_type = command_parser<false, As...>;

    using command_invoker_type = command_invoker<syntax_type, As...>;
  };

  template<typename Authorizer, typename... As>
  struct command_syntax_traits<true, Authorizer, As...>
  {
    using syntax_type = command_syntax<Authorizer, true, As...>;

    using remaining_args_type = std::vector<std::string>;

    using args_type = args<As..., remaining_args_type>;

    using command_parser_type = command_parser<true, As...>;

    using command_invoker_type = command_invoker<syntax_type, As..., remaining_args_type>;
  };

  template<typename Authorizer, bool CaptureRemaining, typename... As>
  class command_syntax : public syntax_base
  {
  public:
    static constexpr bool capture_remaining = CaptureRemaining;

    using traits = command_syntax_traits<capture_remaining, Authorizer, As...>;
    using function_type = handler_function_t<capture_remaining, T, As...>;
    using args_type = typename traits::args_type;
    using command_parser_type = typename traits::command_parser_type;
    using command_invoker_type = typename traits::command_invoker_type;

    command_syntax(T &command, Authorizer &&authorizer)
      : command_{command},
        authorizer_{std::move(authorizer)},
        expression_{ }
    { }

    command_syntax &set_function(function_type function)
    {
      handler_function_ = function;
      return *this;
    }

    command_syntax &set_expression(std::string expression)
    {
      expression_ = std::move(expression);
      return *this;
    }

    std::unique_ptr<command_invoker_base> parse(const std::string &chat_line) override
    {
      command_parser_type parser{expression_};

      auto args = parser.parse(chat_line);
      return std::make_unique<command_invoker_type>(*this, std::move(args));
    }

    bool operator()(const irc::context &context, const irc::protocol::message &message, args_type &args)
    {
      if (!authorizer_.is_authorized(context, message))
        return false;

      return args.apply(handler_function_, command_, context);
    }

  private:
    T &command_;
    function_type handler_function_ = nullptr;

    Authorizer authorizer_;
    std::string expression_;
  };

  module_type &module_;
  std::forward_list<std::unique_ptr<syntax_base>> available_syntaxes_{ };

  template<bool CaptureRemaining, typename... As, typename Authorizer, typename Handler>
  void add_syntax_impl(const std::string &syntax_expr,
    Handler &&function,
    Authorizer &&authorizer = void_authorizer{ })
  {
    auto syntax = std::make_unique<command_syntax<Authorizer, CaptureRemaining, As...>>(
      *reinterpret_cast<T *>(this),
      std::move(authorizer));
    syntax->set_function(std::forward<Handler>(function));
    syntax->set_expression(syntax_expr);

    available_syntaxes_.push_front(std::move(syntax));
  }
};


}
