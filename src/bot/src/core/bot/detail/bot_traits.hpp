//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "connect_strategy.hpp"
#include "capability_negotiation_strategy.hpp"
#include "../../irc/client.hpp"

#include <boost/asio/ip/tcp.hpp>

namespace core::bot::detail
{

template<typename Socket = boost::asio::ip::tcp::socket>
struct bot_traits
{
  using socket_type = Socket;

  using connect_strategy = detail::connect_strategy<socket_type>;
  using client_type = irc::client<socket_type, connect_strategy>;

  using capability_negotiation_strategy = detail::capability_negotiation_strategy<client_type>;
};

using ssl = bot_traits<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>;

}
