//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <core/module/command.hpp>

namespace modules::poll::commands
{

template<typename Module>
class vote_command : public core::module::command<vote_command<Module>, Module>
{
public:
  using core::module::command<vote_command, Module>::command;

  std::string_view get_name() const override
  {
    return getenv("CHATBOT_MOD_POLL_VOTE_COMMAND");
  }

  bool enable() override
  {
    this->template add_syntax<std::string>(
      "<name>",
      &vote_command::vote_by_name);

    this->template add_syntax<uint>(
      "<id>",
      &vote_command::vote_by_id);

    return true;
  }

private:
  bool vote_by_id(const core::irc::context &context, uint option_no)
  {
    auto &poll = this->get_module().get_poll();

    // If there is no poll in progress, ignore the command silently.
    if (!this->get_module().has_poll())
      return false;

    // Tell the user if the poll is already closed.
    if (!poll.is_open()) {
      // Tell the user its closed.
      context.channel_message_to_user("Sorry, this poll has already been closed.");
      return false;
    }

    // Submit the vote
    uint option_id = option_no - 1;
    switch (auto result = poll.submit_vote(context.get_user(), option_id); result) {
    case vote_result::change_vote_not_allowed:
      context.channel_message_to_user("Sorry, you may not change your vote in this poll.");
      break;

    case vote_result::invalid_value:
      context.channel_message_to_user("Invalid value, vote ignored.");
      break;

    case vote_result::success:
      break;
    }

    return true;
  }

  bool vote_by_name(const core::irc::context &context, const std::string &name)
  {
    // If there is no poll in progress, ignore the command silently.
    if (!this->get_module().has_poll())
      return false;

    const auto &poll = this->get_module().get_poll();
    const auto &options = poll.get_options();

    if (auto option_iterator = std::find(std::begin(options), std::end(options), name);
      option_iterator != std::end(options)) {
      vote_by_id(context, option_iterator - std::begin(options));
      return true;
    }

    context.channel_message_to_user("Invalid option, vote ignored.");
    return true;
  }
};

}
