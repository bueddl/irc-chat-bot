//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "plugin.hpp"
#include "detail/loader_traits.hpp"

#include <string>
#include <type_traits>

namespace core::plugin_system
{

template<typename _LoaderTraits = detail::default_loader_traits>
class plugin_loader
{
public:
  using traits_type = _LoaderTraits;
  using handle_type = typename traits_type::handle_type;

  plugin_loader() = default;

  plugin_loader(plugin_loader &&__r) noexcept
    : handle_{std::__exchange(__r.handle_, nullptr)}
  { }

  virtual ~plugin_loader()
  {
    if (is_open())
      close();
  }

  bool is_open() const noexcept
  { return handle_ != nullptr; }

  void open(const std::string &filename)
  {
    handle_ = traits_type::open(filename);
  }

  template<typename T>
  auto resolve_symbol(const std::string &symbol)
  {
    auto address = traits_type::resolve_symbol(handle_, symbol);
    return reinterpret_cast<std::add_pointer_t<T>>(address);
  }

  void close()
  {
    traits_type::close(handle_);
    handle_ = nullptr;
  }

private:
  handle_type handle_ = nullptr;
};

}
