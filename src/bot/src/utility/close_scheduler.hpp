//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <boost/asio/basic_waitable_timer.hpp>

#include <chrono>

namespace utility
{

class close_scheduler
{
public:
  using clock = std::chrono::system_clock;
  using timer = boost::asio::basic_waitable_timer<clock>;
  using time_point = typename clock::time_point;

  explicit close_scheduler(boost::asio::io_service &io)
    : io_{io},
      timer_{io_}
  { }

  template<typename Rep, typename Period, typename IntervalHandler>
  void close(std::chrono::duration<Rep, Period> duration,
    IntervalHandler &&interval_handler)
  {
    cancel();
    in_progress_ = true;

    interval_handler_ = interval_handler;
    expire_time_ = clock::now() + duration;
    initialize_timer();

    interval_handler_();
  }

  void cancel()
  {
    if (!in_progress_)
      return;

    timer_.cancel();
    in_progress_ = false;
  }

  auto time_to_expire() const
  {
    return expire_time_ - clock::now();
  }

  bool is_in_progress() const
  { return in_progress_; }

private:
  boost::asio::io_service &io_;

  bool in_progress_ = false;

  timer timer_;
  time_point expire_time_;

  std::function<void()> interval_handler_;

  void initialize_timer()
  {
    using namespace std::chrono_literals;

    auto interval = expire_time_ - clock::now();

    auto gaps = {60s, 15s, 5s};

    for (const auto &gap : gaps) {
      if (interval <= gap)
        continue;

      if (auto shift_seconds = interval % gap; shift_seconds != 0s)
        interval = shift_seconds;
      else
        interval = gap;
      break;
    }

    timer_.expires_from_now(interval);

    timer_.async_wait(
      [this] (const auto &ec)
      {
        if (ec || !in_progress_)
          return;

        using namespace std::chrono_literals;
        if (expire_time_ - clock::now() > 0s)
          initialize_timer();
        else
          in_progress_ = false;

        interval_handler_();
      });
  }
};

}
