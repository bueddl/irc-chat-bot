//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "commands/bot_command.hpp"
#include "../../core/module/module.hpp"

#include <string>

using namespace std::string_view_literals;

namespace modules::builtin
{

template<typename Bot>
class builtin_module : public core::module::module<builtin_module<Bot>>
{
public:
  using bot_type = Bot;

  explicit builtin_module(boost::asio::io_service &io, bot_type &bot)
    : core::module::module<builtin_module<Bot>>{io},
      bot_{bot}
  { }

  std::string_view get_name() override
  {
    return "builtin"sv;
  }

  std::string_view get_version() override
  {
    return "0.1.1"sv;
  }

  void create() override
  {
    this->template register_command<commands::bot_command<builtin_module<Bot>, bot_type>>(bot_);
  }

private:
  bot_type &bot_;
};

}
