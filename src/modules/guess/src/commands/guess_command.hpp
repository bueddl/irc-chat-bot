//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "../bet.hpp"

#include <utility/close_scheduler.hpp>
#include <core/module/command.hpp>
#include <twitch/usertype_authorizer.hpp>

#include <chrono>

namespace modules::guess::commands
{

template<typename Module>
class guess_command : public core::module::command<guess_command<Module>, Module>
{
public:
  static constexpr std::size_t default_max_display_count = 5;

  //using command::command;
  explicit guess_command(Module &module)
    : core::module::command<guess_command, Module>::command{module}
  { }

  std::string_view get_name() const override
  {
    return getenv("CHATBOT_MOD_GUESS_COMMAND");
  }

  bool enable() override
  {
    this->add_syntax(
      &guess_command::display_help);

    this->add_syntax(
      &guess_command::display_help_always,
      twitch::usertype_authorizer{twitch::usertype::mod});

    this->template add_syntax<double>(
      "<number>",
      &guess_command::place_bet);

    this->add_syntax(
      "open",
      &guess_command::open,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<uint>(
      "open <delay>",
      &guess_command::open,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->add_syntax(
      "reopen",
      &guess_command::reopen,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<uint>(
      "reopen <delay>",
      &guess_command::reopen,
      twitch::usertype_authorizer{twitch::usertype::mod});

    this->add_syntax(
      "close",
      &guess_command::close,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<uint>(
      "close <delay>",
      &guess_command::close,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->add_syntax(
      "close cancel",
      &guess_command::cancel_close,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->add_syntax(
      "close now",
      &guess_command::close_now,
      twitch::usertype_authorizer{twitch::usertype::mod});

    this->template add_syntax<double>(
      "result <number>",
      &guess_command::set_result,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->add_syntax(
      "result list",
      &guess_command::display_results,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->add_syntax(
      "winner pick",
      &guess_command::pick_random_winner,
      twitch::usertype_authorizer{twitch::usertype::mod});

    return true;
  }

private:
  bool display_help(const core::irc::context &context)
  {
    auto &bet = this->get_module().get_bet();

    if (!this->get_module().has_bet() || !bet.is_open())
      return false;

    return display_help_always(context);
  }

  bool display_help_always(const core::irc::context &context)
  {
    std::ostringstream message;
    message << "Place your bet by using the !guess <your-guess> command.";

    auto &bet = this->get_module().get_bet();

    if (this->get_module().has_bet() && bet.is_ignoring_sign())
      message << " Any leading sign will be ignored.";

    context.channel_message(message);
    return true;
  }

  bool place_bet(const core::irc::context &context, double guess)
  {
    auto &bet = this->get_module().get_bet();

    // If there is no bet in progress, ignore the command silently.
    if (!this->get_module().has_bet())
      return false;

    // Tell the user if the bet is already closed.
    if (!bet.is_open()) {
      if (bet.is_evaluated())
        // Already evaluated, so it's definitely done.
        context.channel_message_to_user("The bet has already taken place.");
      else
        // Tell the user its closed.
        context.channel_message_to_user("Sorry, this bet has already been closed.");
      return false;
    }

    // Place the guess
    switch (auto result = bet.place(context.get_user(), guess); result) {
    case place_bet_result::change_guess_not_allowed:
      context.channel_message_to_user("Sorry, changing your guess is not allowed in this bet.");
      break;

    case place_bet_result::duplicate_guess_not_allowed:
      context.channel_message_to_user("Sorry, guesses must be unique in this bet.");
      break;

    case place_bet_result::invalid_value:
      context.channel_message_to_user("Invalid value, guess ignored.");
      break;

    case place_bet_result::success:
      break;
    }

    return true;
  }

  bool open(const core::irc::context &context, uint close_delay)
  {
    // Is there a guess still in progress or not evaluated yet (aka. the "Bruno"-check)
    if (this->get_module().has_bet() && !this->get_module().get_bet().is_evaluated()) {
      context.channel_message_to_user("There is still an unevaluated bet in progress!");
      return false;
    }

    this->get_module().reset_bet();

    auto &bet = this->get_module().get_bet();
    bet.open();
    context.channel_message("✈ ✈ ✈ Bet is open now. ✈ ✈ ✈ ", true);
    if (close_delay > 0)
      close(context, close_delay);

    return true;
  }

  bool open(const core::irc::context &context)
  {
    return open(context, 0);
  }

  bool reopen(const core::irc::context &context, uint close_delay)
  {
    auto &bet = this->get_module().get_bet();

    // If there is no bet in progress, tell the invoking user.
    if (!this->get_module().has_bet()) {
      context.channel_message_to_user("There is no bet taking place right now.");
      return false;
    }

    // Check if the bet is still open
    if (bet.is_open()) {
      context.channel_message_to_user("This bet is stll open.");
      return true;
    }

    bet.reopen();
    if (close_delay > 0)
      close(context, close_delay);

    // Good news - tell the people!
    context.channel_message("⭐ ⭐ ⭐ The bet has been opened again for guessing. ⭐ ⭐ ⭐", true);

    return true;
  }

  bool reopen(const core::irc::context &context)
  {
    return reopen(context, 0);
  }

  bool close(const core::irc::context &context, uint delay)
  {
    auto &bet = this->get_module().get_bet();

    // If there is no bet in progress, tell the invoking user.
    if (!this->get_module().has_bet()) {
      context.channel_message_to_user("There is no bet taking place right now.");
      return false;
    }

    auto &close_scheduler = this->get_module().get_close_scheduler();
    if (close_scheduler.is_in_progress())
      close_scheduler.cancel();

    // close immediately
    if (delay <= 0) {
      bet.close();
      std::ostringstream message;
      message << "✈ ✈ ✈ Bet is closed now. ✈ ✈ ✈ "
        << bet.count()
        << " guesses made";
      context.channel_message(message, true);
      return true;
    }

    close_scheduler.close(
      std::chrono::seconds{delay},
      [this, context]
      {
        using namespace std::chrono_literals;

        auto &close_scheduler = this->get_module().get_close_scheduler();
        if (close_scheduler.time_to_expire() > 0s) {
          std::ostringstream message;
          message << "Bet will be closed in "
                  << std::chrono::round<std::chrono::seconds>(close_scheduler.time_to_expire()).count()
                  << " seconds!";
          context.channel_message(message, true);
          return;
        }

        close_now(context);
      }
    );

    return true;
  }

  bool close(const core::irc::context &context)
  {
    return close(context, 60);
  }

  bool close_now(const core::irc::context &context)
  {
    return close(context, 0);
  }

  bool cancel_close(const core::irc::context &context)
  {
    // If there is no bet in progress, tell the invoking user.
    if (!this->get_module().has_bet()) {
      context.channel_message_to_user("There is no bet taking place right now.");
      return false;
    }

    auto &close_scheduler = this->get_module().get_close_scheduler();
    if (!close_scheduler.is_in_progress()) {
      context.channel_message_to_user("This bet is not about to be closed.");
      return false;
    }

    close_scheduler.cancel();
    context.channel_message("Closing of bet canceled.");
    return true;
  }

  bool set_result(const core::irc::context &context, double value)
  {
    // If there is no bet in progress, tell the invoking user.
    if (!this->get_module().has_bet()) {
      context.channel_message_to_user("There is no bet taking place right now.");
      return false;
    }

    auto &bet = this->get_module().get_bet();

    bet.set_result(value);
    display_results(context);

    return true;
  }

  bool pick_random_winner(const core::irc::context &context)
  {
    // If there is no bet in progress, tell the invoking user.
    if (!this->get_module().has_bet()) {
      context.channel_message_to_user("There is no bet taking place right now.");
      return false;
    }

    auto &bet = this->get_module().get_bet();

    // If the bet is still in progress...
    if (bet.is_open()) {
      context.channel_message_to_user("This bet is still opened for guesses!");
      return false;
    }

    if (!bet.is_evaluated()) {
      context.channel_message_to_user("This bet has not been evaluated yet!");
      return false;
    }

    context.channel_message_to_user("NotLikeThis Sorry, not yet implemented!");

    return true;
  }

  bool display_results(const core::irc::context &context)
  {
    // If there is no bet in progress, tell the invoking user.
    if (!this->get_module().has_bet()) {
      context.channel_message_to_user("There is no bet taking place right now.");
      return false;
    }

    auto &bet = this->get_module().get_bet();

    // If the bet is still in progress...
    if (bet.is_open()) {
      context.channel_message_to_user("This bet is still opened for guesses!");
      return false;
    }

      if (!bet.is_evaluated()) {
        context.channel_message_to_user("This bet has not been evaluated yet!");
        return false;
    }

    auto results = bet.get_results();

    // No guesses made?
    if (results.size() == 0) {
      context.channel_message("No guesses were made!");
      return true;
    }

    auto display_count = default_max_display_count;
    if (auto count = results.size(); count < default_max_display_count)
      display_count = count;

    auto result_value = bet.get_result();

    std::ostringstream message;
    message << "✈ ✈ ✈ Landing rate: "
            << result_value
            << "fpm, top "
            << display_count
            << " bet results: ";

    int place = 1, index = 0;
    double previous_value = results.begin()->first;

    for (const auto &result : results) {
      double delta = result.second.delta_to(
        result_value,
        bet.is_ignoring_sign()
      );

      ++index;
      if (result.first != previous_value) {
        previous_value = result.first;
        place = index;
      }

      message << place
              << ". "
              << result.second.get_user().display_name()
              << " ("
              << std::showpos << delta << std::noshowpos
              << "fpm)";
      if (index >= display_count)
        break;

      message << " | ";
    }

    context.channel_message(message, true);
    return true;
  }
};

}
