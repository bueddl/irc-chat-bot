//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <core/irc/user.hpp>

#include <optional>
#include <map>
#include <vector>

namespace modules::poll
{

enum class vote_result
{
  success,
  change_vote_not_allowed,
  invalid_value
};

class poll
{
public:
  class vote
  {
  public:
    vote(const core::irc::user &user, uint option_id)
      : user_{user},
        option_id_{option_id}
    { }

    vote(const vote &) = default;
    vote& operator=(const vote &) = default;

    void update_value(double option_id)
    { option_id_ = option_id; }

    uint get_option_id() const
    { return option_id_; }

    const auto& get_user() const
    { return user_; }

  private:
    const core::irc::user user_;
    uint option_id_;
  };

  poll(const std::string &text, const std::vector<std::string> &options)
    : text_{text},
      options_{options}
  { }

  void open();
  void reopen();
  void close();

  const std::vector<std::string>& get_options() const
  { return options_; }

  vote_result submit_vote(const core::irc::user &user, uint option_id);

  std::vector<std::pair<uint, std::size_t>> get_results() const;

  std::size_t vote_count() const
  { return votes_.size(); }

  bool is_open() const
  { return open_; }

private:
  std::string text_;
  std::vector<std::string> options_;
  std::vector<vote> votes_;

  bool open_ = false;
  bool update_allowed_ = false;
};

}
