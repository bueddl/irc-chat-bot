//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <string>

#ifdef linux

#include <dlfcn.h>

#elif defined(_WIN32)

#include <windows.h>

#endif

namespace core::plugin_system::detail
{

#ifdef linux
// Linux implementation

struct default_loader_traits
{
  using handle_type = void *;
  using address_type = void *;

  static handle_type open(const std::string &filename)
  {
    return dlopen(filename.c_str(), RTLD_NOW);
  }

  static void close(handle_type handle)
  {
    dlclose(handle);
  }

  static address_type resolve_symbol(handle_type handle, const std::string &symbol)
  {
    return dlsym(handle, symbol.c_str());
  }
};

#elif defined(_WIN32)
// Windows implementation

struct default_loader_traits
{
  using handle_type = HMODULE;
  using address_type = FARPROC;

  static handle_type open(const std::string &filename)
  {
    return LoadLibraryA(filename.c_str());
  }

  static void close(handle_type handle)
  {
    FreeLibrary(handle);
  }

  static address_type resolve_symbol(handle_type handle, const std::string &symbol)
  {
    return GetProcAddress(handle, symbol.c_str());
  }
};

#else
// No implementation for others... yet!

static_assert("plugin_loader is implemented only for linux or windows platform.");
#endif

}
