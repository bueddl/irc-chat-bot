//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "poll.hpp"
#include "commands/poll_command.hpp"
#include "commands/vote_command.hpp"

#include <core/module/module.hpp>
#include <utility/close_scheduler.hpp>

using namespace std::string_view_literals;

namespace modules::poll
{

class poll_module : public core::module::module<poll_module>
{
public:
  explicit poll_module(boost::asio::io_service &io)
    : core::module::module<poll_module>{io},
      close_scheduler_{io}
  { }

  std::string_view get_name() override
  {
    return "poll"sv;
  }

  std::string_view get_version() override
  {
    return "0.1.19"sv;
  }

  void create() override
  {
    register_command<commands::poll_command>();
    register_command<commands::vote_command>();
  }

  bool has_poll() const
  { return static_cast<bool>(poll_); }

  poll& get_poll()
  { return *(poll_.get()); }

  void create_poll(const std::string &text, const std::vector<std::string> &options)
  {
    poll_ = std::make_unique<poll>(text, options);
  }

  utility::close_scheduler& get_close_scheduler()
  { return close_scheduler_; }

private:
  std::unique_ptr<poll> poll_;
  utility::close_scheduler close_scheduler_;
};

}
