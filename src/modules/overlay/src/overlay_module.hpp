//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "commands/overlay_command.hpp"

#include <core/module/module.hpp>

using namespace std::string_view_literals;

namespace modules::overlay
{

class overlay_module : public core::module::module<overlay_module>
{
public:
  using core::module::module<overlay_module>::module;

  std::string_view get_name() override
  {
    return "overlay"sv;
  }

  std::string_view get_version() override
  {
    return "0.1.1"sv;
  }

  void create() override
  {
    register_command<commands::overlay_command>();
  }
};

}
