//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "protocol/message.hpp"
#include "client.hpp"

#include <string>

namespace core::irc
{

using command = protocol::command;

class channel
{
public:
  channel(client_base &cl, std::string name)
    : client_{cl},
      name_{std::move(name)}
  { }

  const std::string &name() const
  {
    return name_;
  }

  void message(std::string text, bool colorize = false) const
  {
    using namespace std::string_literals;

    if (colorize)
      text.insert(0, "/me "s);
    client_.send(command::channel_message, name_, text);
  }

  void message(const std::ostringstream &stream, bool colorize = false) const
  {
    message(stream.str(), colorize);
  }

  void message_to_user(const std::string &text, const user &usr, bool colorize = false) const
  {
    std::ostringstream stream;
    stream << usr.display_name() << " → " << text;
    message(stream, colorize);
  };

  void message_to_user(const std::ostringstream &stream, const user &usr, bool colorize = false) const
  {
    message_to_user(stream.str(), usr, colorize);
  }

private:
  client_base &client_;
  const std::string name_;
};

}
