//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "../../irc/protocol/message.hpp"
#include "../../module/command.hpp"

#include <boost/bind.hpp>

#include <vector>
#include <string>
#include <iostream>
#include <experimental/iterator>

namespace core::bot::detail
{

template<typename Client>
class capability_negotiation_strategy
{
public:
  using client_type = Client;
  using client_base = irc::client_base;

  explicit capability_negotiation_strategy(client_type &client)
    : client_{client}
  { }

  template<typename... Args>
  void negotiate(Args &&... args)
  {
    requested_capabilities_.insert(
      requested_capabilities_.end(),
      {std::forward<Args>(args)...}
    );
    perform_stage1();
  }

private:
  client_type &client_;

  std::vector<std::string> requested_capabilities_;
  std::vector<std::string> supported_capabilities_;
  std::vector<std::string> acknowledged_capabilities_;

  void perform_stage1()
  {
    using irc::protocol::command;

    client_.send(command::capability, "LS");
    client_.once(command::capability,
      boost::bind(
        &capability_negotiation_strategy::perform_stage2, this,
        _1, _2));
  }

  void perform_stage2(client_base &client, const irc::protocol::message &message)
  {
    std::istringstream capabilities{message.get_params()[2]};
    std::cout << "Capabilities supported: " << capabilities.str() << std::endl;

    std::copy(std::istream_iterator<std::string>(capabilities),
      std::istream_iterator<std::string>(),
      std::back_inserter(supported_capabilities_));

    using irc::protocol::command;
    for (const auto &cap : requested_capabilities_)
      if (std::find(supported_capabilities_.begin(), supported_capabilities_.end(), cap)
          != std::end(supported_capabilities_))
        client_.send(command::capability, "REQ", cap);

    client_.on(command::capability,
      boost::bind(
        &capability_negotiation_strategy::perform_stage3, this,
        _1, _2));
  }

  void perform_stage3(client_base &client, const irc::protocol::message &message)
  {
    std::istringstream capabilities{message.get_params()[2]};
    std::cout << "Capabilities acknowledged: " << capabilities.str() << std::endl;

    std::copy(std::istream_iterator<std::string>(capabilities),
      std::istream_iterator<std::string>(),
      std::back_inserter(acknowledged_capabilities_));
  }
};

}
