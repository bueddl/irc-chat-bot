//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <core/irc/user.hpp>

namespace modules::guess
{

enum class place_bet_result
{
  success,
  change_guess_not_allowed,
  duplicate_guess_not_allowed,
  invalid_value
};

class bet
{
public:
  class guess
  {
  public:
    guess(const core::irc::user &user, double value)
      : user_{user},
        value_{value}
    { }

    guess(const guess &) = default;
    guess& operator=(const guess &) = default;

    void update_value(double value)
    { value_ = value; }

    double get_value() const
    { return value_; }

    double delta_to(double target, bool ignore_signs = false) const
    { return std::abs(target) - std::abs(value_); }

    const auto& get_user() const
    { return user_; }

  private:
    const core::irc::user user_;
    double value_;
  };

  bet() = default;

  void open();
  void reopen();
  void close();

  place_bet_result place(const core::irc::user &user, double value);

  void set_result(double value);

  std::multimap<double, guess> get_results();
  void pick_random_winner();

  std::size_t count() const
  { return guesses_.size(); }

  double get_result() const
  { return results_.value().value; }

  bool is_ignoring_sign() const
  { return ignore_sign_; }

  bool is_open() const
  { return open_; }

  bool is_evaluated() const
  { return results_.has_value(); }

private:
  template<typename Key, typename Value>
  using unordered_map = std::unordered_map<Key, Value, core::irc::hash>;

  struct result_info
  {
    double value;
    std::multimap<double, guess> list;
  };

  unordered_map<core::irc::user, guess> guesses_;
  std::optional<result_info> results_;

  bool open_ = false;

  bool ignore_sign_ = true;
  bool require_unique_ = false;
  bool update_allowed_ = false;
};

}
