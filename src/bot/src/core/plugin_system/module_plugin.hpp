//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "plugin.hpp"
#include "../module/module.hpp"

#include <memory>

namespace core::plugin_system
{

template<typename T>
class module_plugin : public plugin
{
public:
  load_state load(module::module_manager &module_manager) override
  {
    module_ = module_manager.register_module<T>();
    module_->create();
    return load_state::keep;
  }

  std::string_view get_name() override
  {
    return module_->get_name();
  }

  bool enable() override
  {
    return module_->enable();
  }

  void disable() override
  { }

  void unload() override
  { }

private:
  std::shared_ptr<module::module_base> module_;
};

}
