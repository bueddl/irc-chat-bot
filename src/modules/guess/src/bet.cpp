//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "bet.hpp"

namespace modules::guess
{


void bet::open()
{
  open_ = true;

  guesses_.clear();
  results_.reset();
}

void bet::reopen()
{
  open_ = true;
}

void bet::close()
{
  open_ = false;
}

place_bet_result bet::place(const core::irc::user &user, double value)
{
  if (require_unique_) {
    if (auto same_value_iterator = std::find_if(
        std::begin(guesses_), std::end(guesses_),
        [value](auto guess) { return value == guess.second.get_value(); });
      same_value_iterator != std::end(guesses_))
      if (same_value_iterator->first == user)
        // user submitted same value before, ignore...
        return place_bet_result::success;
      else
        return place_bet_result::duplicate_guess_not_allowed;
  }

  if (!update_allowed_) {
    if (auto guess_iterator = guesses_.find(user); guess_iterator != std::end(guesses_))
      if (guess_iterator->second.get_value() == value)
        // same value already guessed by user, ignore...
        return place_bet_result::success;
      else
        return place_bet_result::change_guess_not_allowed;
  }

  guesses_.emplace(user, guess{user, value});

  return place_bet_result::success;
}

void bet::set_result(double value)
{
  result_info result;
  result.value = value;

  for (const auto &guess : guesses_) {
    double delta = std::abs(
      guess.second.delta_to(result.value,
        ignore_sign_));

    result.list.emplace(
      delta,
      guess.second);
  }

  results_ = result;
}

std::multimap<double, bet::guess> bet::get_results()
{
  return results_.value().list;
}

void bet::pick_random_winner()
{

}
}
