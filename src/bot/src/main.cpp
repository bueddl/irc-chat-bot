//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#include "core/bot/bot.hpp"
#include "core/bot/password_nick_authenticator.hpp"

int main()
{
  boost::asio::io_service io;
  core::bot::bot bot{io};

  bot.load_default_modules();
  bot.load_plugins();

  auto irc_host = ::getenv("CHATBOT_IRC_HOST");
  auto irc_port_str = ::getenv("CHATBOT_IRC_PORT");
  auto irc_port = ::atoi(irc_port_str);
  auto irc_login = ::getenv("CHATBOT_IRC_LOGIN");
  auto irc_password = ::getenv("CHATBOT_IRC_PASSWORD");
  auto irc_channel = ::getenv("CHATBOT_IRC_CHANNEL");

  bot.connect(irc_host, irc_port,
    core::bot::password_nick_authenticator{
    irc_password,
      irc_login
    },
    irc_channel);

  io.run();

  return 0;
}
