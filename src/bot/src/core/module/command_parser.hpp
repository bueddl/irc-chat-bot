//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "args.hpp"

#include <string>

namespace core::module
{

template<bool, typename... Ps>
struct command_parser_traits
{
  using args_type = args<Ps...>;
};

template<typename... Ps>
struct command_parser_traits<true, Ps...>
{
  using args_type = args<Ps..., std::vector<std::string>>;

  static constexpr std::size_t args_pos = sizeof...(Ps);
};

template<bool CaptureRemaining, typename... Ps>
class command_parser
{
public:
  static constexpr bool capture_remaining = CaptureRemaining;

  using traits = command_parser_traits<capture_remaining, Ps...>;
  using args_type = typename traits::args_type;

  explicit command_parser(std::string expr)
    : expr_{std::move(expr)},
      str_{}
  { }

  args_type parse(std::string str)
  {
    pos_ = expr_pos_ = 0;
    str_ = std::move(str);

    parse_whitespace();

    // Only pass required args, not the optional remaining-args
    bool valid = parse_expression<0, Ps...>();

    parse_whitespace();

    if constexpr (capture_remaining) {
      std::vector<std::string> args;
      std::string arg;
      while (parse_param(arg)) {
        args.emplace_back(std::move(arg));
        parse_whitespace();
      }
      args_.template set_value<traits::args_pos>(args);
    } else {
      if (pos_ < str_.size())
        valid = false;
    }

    args_.is_valid(valid);

    return args_;
  }

private:
  const std::string expr_;
  std::size_t expr_pos_ = 0;

  std::string str_;
  std::size_t pos_ = 0;

  args_type args_{ };

  template<char Char, char... Chars>
  bool one_of(char ch) const
  {
    if (Char == ch)
      return true;

    if constexpr (sizeof...(Chars) > 0)
      return one_of<Chars...>(ch);

    return false;
  }

  bool is_special(char ch) const
  {
    return one_of<'<', '>', '[', ']'>(ch);
  }

  bool is_whitespace(char ch) const
  {
    return one_of<' ', '\t'>(ch);
  }

  template<char A, char B>
  bool between(char ch) const
  {
    return A <= ch && ch <= B;
  };

  bool is_digit(char ch) const
  {
    return between<'0', '9'>(ch);
  }

  bool is_keyword(char ch) const
  {
    return between<'a', 'z'>(ch)
           || between<'A', 'Z'>(ch)
           || is_digit(ch)
           || one_of<'_', '-'>(ch);
  }

  bool is_end_of_input() const
  { return str_.size() <= pos_; }

  char current() const
  {
    return str_[pos_];
  }

  void seek()
  {
    ++pos_;
  }

  char current_seek()
  {
    return str_[pos_++];
  }

  bool parse_whitespace()
  {
    if (!is_whitespace(current()))
      return false;

    do
      seek();
    while (is_whitespace(current()));

    return true;
  }

  bool parse_param(double &param)
  {
    param = 1;
    if (current() == '-') {
      param = -1;
      seek();
    } else if (current() == '+')
      seek();

    bool valid = false;
    double value = 0;
    for (; current() != '.'; seek()) {
      if (!is_digit(current())) {
        param *= value;
        return valid;
      }
      value *= 10;
      value += current() - '0';
      valid = true;
    }
    seek();
    for (double weight = 10; is_digit(current()); weight *= 10, seek()) {
      value += (current() - '0') / weight;
      valid = true;
    }

    param *= value;
    return valid;
  }

  template<typename T, typename = std::enable_if_t<std::is_integral_v<T>>>
  bool parse_param(T &param)
  {
    param = 0;

    double sign = 1;
    if constexpr (!std::is_unsigned_v<T>) {
      if (current() == '-') {
        sign = -1;
        seek();
      } else if (current() == '+')
        seek();
    }

    if (!is_digit(current()))
      return false;

    do {
      param *= 10;
      param += current() - '0';
      seek();
    } while (is_digit(current()));

    param *= sign;
    return true;
  }

  bool parse_param(std::string &string)
  {
    string.clear();
    while (!(is_end_of_input() || is_whitespace(current())))
      string += current_seek();

    return !string.empty();
  }

  template<std::size_t I, typename Param, typename... Rs>
  bool parse_parameter_expression()
  {
    while (expr_[++expr_pos_] != '>');
    Param param;
    if (!parse_param(param))
      return false;

    args_.template set_value<I>(param);

    return parse_expression<I + 1, Rs...>();
  }

  template<std::size_t I = 0, typename... As>
  bool parse_expression()
  {
    while (expr_pos_ + 1 < expr_.size()) {
      // for each whitespace, skip whitespace in chat_line. if there is no ws, abort.
      if (is_whitespace(expr_[expr_pos_])) {
        while (is_whitespace(expr_[++expr_pos_]));
        if (!parse_whitespace())
          return false;
        continue;
      }

      if (is_special(expr_[expr_pos_])) {
        // if '<', skip until '>' and take next template parameter to extract for

        if (expr_[expr_pos_] == '<') {
          if constexpr (sizeof...(As) == 0)
            return false; // error: unexpected parameter where no type is
                          // provided
          else
            return parse_parameter_expression<I, As...>(); // recurses to omit
                                                           // active parameter
        }

        // invalid special char
        return false;
      }

      // for each non-whitespace, non special, match 1 by 1, if mismatch, abort.
      if (is_keyword(expr_[expr_pos_])) {
        do {
          if (current_seek() != expr_[expr_pos_++])
            return false;
        }  while (is_keyword(expr_[expr_pos_]));

        continue;
      }

      // invalid?
      return false;
    }

    // TODO: how to implement optional clause [ ... ]

    return true;
  }
};

}
