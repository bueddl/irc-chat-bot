//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <core/module/command.hpp>
#include <twitch/usertype_authorizer.hpp>

#include <algorithm>
#include <iostream>
#include <chrono>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

namespace modules::overlay::commands
{

template<typename Module>
class overlay_command : public core::module::command<overlay_command<Module>, Module>
{
public:
  using core::module::command<overlay_command, Module>::command;

  std::string_view get_name() const override
  {
    return getenv("CHATBOT_MOD_OVERLAY_COMMAND");
  }

  bool enable() override
  {
    this->template add_syntax<std::string>(
      "arr <icao>",
      &overlay_command::set_arr,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<std::string>(
      "dep <icao>",
      &overlay_command::set_dep,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<std::string>(
      "callsign <callsign>",
      &overlay_command::set_callsign,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<std::string>(
      "atc <network>",
      &overlay_command::set_atc,
      twitch::usertype_authorizer{twitch::usertype::mod});
    this->template add_syntax<std::string, std::string>(
      "eta <arrival-time> <timezone>",
      &overlay_command::set_eta,
      twitch::usertype_authorizer{twitch::usertype::mod});

    this->template add_syntax<std::string>(
      "eta <arrival-time>",
      &overlay_command::set_eta,
      twitch::usertype_authorizer{twitch::usertype::mod});

    this->template add_syntax_with_capture(
      "sim",
      &overlay_command::set_sim,
      twitch::usertype_authorizer{twitch::usertype::mod});

    return true;
  }

private:
  bool set_arr(const core::irc::context &context, const std::string &icao)
  {
    update_info("arr", icao);
    return true;
  }

  bool set_dep(const core::irc::context &context, const std::string &icao)
  {
    update_info("dep", icao);
    return true;
  }

  bool set_callsign(const core::irc::context &context, const std::string &callsign)
  {
    update_info("callsign", callsign);
    return true;
  }

  bool set_atc(const core::irc::context &context, const std::string &network)
  {
    update_info("atc", network);
    return true;
  }

  bool set_eta(const core::irc::context &context, const std::string &arrival_time)
  {
    return set_eta(context, arrival_time, "UTC");
  }

  bool set_eta(const core::irc::context &context, const std::string &arrival_time, const std::string &timezone)
  {
    update_info("eta", arrival_time + " " + timezone);
    return true;
  }

  bool set_sim(const core::irc::context &context, const std::vector<std::string> &args)
  {
    std::ostringstream simulator;
    copy(begin(args), end(args), std::ostream_iterator<std::string>{simulator, " "});
    update_info("sim", simulator.str());
    return true;
  }

  void update_info(const std::string &info, const std::string &value)
  {
    rapidjson::Document d;
    d.SetObject();

    rapidjson::Value xinfo;
    xinfo.SetString(info.c_str(), info.size());
    d.AddMember("info", xinfo, d.GetAllocator());

    rapidjson::Value xvalue;
    xvalue.SetString(value.c_str(), value.size());
    d.AddMember("value", xvalue, d.GetAllocator());

    rapidjson::StringBuffer buffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
    d.Accept(writer);

    std::cout << buffer.GetString() << std::endl;

    try {
      curlpp::Cleanup cleaner;
      curlpp::Easy request;

      request.setOpt(new curlpp::options::Url("https://bot.oneengineout.com/api/modules/overlay/update"));
      request.setOpt(new curlpp::options::Verbose(false));

      std::list<std::string> header;
      header.push_back("Content-Type: application/octet-stream");

      request.setOpt(new curlpp::options::HttpHeader(header));

      request.setOpt(new curlpp::options::PostFields(buffer.GetString()));
      request.setOpt(new curlpp::options::PostFieldSize(buffer.GetSize()));

      request.perform();
      std::ostringstream os;
      //os << request;
    }
    catch ( curlpp::LogicError & e ) {
      std::cout << e.what() << std::endl;
    }
    catch ( curlpp::RuntimeError & e ) {
      std::cout << e.what() << std::endl;
    }
  }

};

}
