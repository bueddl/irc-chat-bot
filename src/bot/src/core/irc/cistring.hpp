//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <string>

namespace core::irc
{

template<typename CharType>
struct char_traits : public std::char_traits<CharType>
{
  static bool eq(CharType c1, CharType c2)
  { return toupper(c1) == toupper(c2); }

  static bool ne(CharType c1, CharType c2)
  { return toupper(c1) != toupper(c2); }

  static bool lt(CharType c1, CharType c2)
  { return toupper(c1) < toupper(c2); }

  static int compare(const CharType *lhs, const CharType *rhs, size_t n)
  {
    while (n-- != 0) {
      if (toupper(*lhs) < toupper(*rhs))
        return -1;
      if (toupper(*lhs) > toupper(*rhs))
        return 1;
      ++lhs;
      ++rhs;
    }
    return 0;
  }

  static const CharType *find(const CharType *haystack, int n, CharType needle)
  {
    while (n-- > 0 && toupper(*haystack) != toupper(needle))
      ++haystack;
    return haystack;
  }
};

template<typename CharType>
using basic_cistring = std::basic_string<CharType, char_traits<CharType>>;

using cistring = basic_cistring<char>;
using ciwstring = basic_cistring<wchar_t>;

}
