//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include <sstream>
#include <iostream>
#include <optional>
#include <map>
#include "message.hpp"

namespace core::irc::protocol
{

class message_parser
{
  using char_type = int;
  using message_type = message;

public:
  explicit message_parser(const std::string &data)
    : stream_{data}
  { }

  bool parse(message_type &msg)
  {
    return parse_message_type(msg);
  }

protected:
  bool parse_message_type(message_type &message)
  {
    //
    // <message_type>       ::= ['@' <tags> <SPACE>] [':' <prefix> <SPACE> ] <command> <params> <crlf>
    //

    if (consume_if('@')) {
      if (!parse_tags(message.get_tags()))
        return false;

      if (!consume_if(' '))
        return false;
    }

    if (consume_if(':')) {
      if (!parse_prefix(message.get_prefix()))
        return false;

      if (!consume_if(' '))
        return false;
    }

    int code;
    if (!parse_command(code))
      return false;
    message.set_code(code);

    if (!parse_params(message.get_params()))
      return false;

    return consume_if('\r') && consume_if('\n');
  }

  bool parse_params(std::vector<std::string> &params)
  {
    //
    // params     =  *14( SPACE middle ) [ SPACE ":" trailing ]
    //            =/ 14( SPACE middle ) [ SPACE [ ":" ] trailing ]
    // Note:
    // This implementation ignores the possibility to omit the colon between the middle and the tailing sequence
    // for parameter lists above 14 params (no command supports that much parameters nowadays).

    while (consume_if(' ')) {
      std::string param;
      if (consume_if(':')) {
        if (!parse_trailing(param))
          return false;
      } else if (!parse_middle(param))
        return false;

      params.emplace_back(std::move(param));
    }

    return true;
  }

  bool parse_middle(std::string &middle)
  {
    //
    // middle     =  nospcrlfcl *( ":" / nospcrlfcl )
    //
    if (!is_nospcrlfcl(stream_.peek()))
      return false;

    middle.clear();
    do
      middle += stream_.get();
    while (is_nospcrlfcl(stream_.peek()) || stream_.peek() == ':');

    return true;
  }

  bool parse_trailing(std::string &trailing)
  {
    //
    // trailing   =  *( ":" / " " / nospcrlfcl )
    //

    if (!(is_nospcrlfcl(stream_.peek()) || any_of<':', ' '>(stream_.peek())))
      return false;

    trailing.clear();
    do
      trailing += stream_.get();
    while (is_nospcrlfcl(stream_.peek()) || any_of<':', ' '>(stream_.peek()));

    return true;
  }

  bool parse_command(int &code)
  {
    //
    // command    =  1*letter / 3digit
    //
    // Note:
    // This doesn't follow the standard very strictly as it theoretically allows numbered commands of indefinite length.

    code = 0;

    if (is_letter(stream_.peek())) {
      cistring command;
      do
        command += stream_.get();
      while (is_letter(stream_.peek()));

      code = static_cast<int>(make_command(command));
      return true;
    }

    if (is_digit(stream_.peek())) {
      do {
        code *= 10;
        code += stream_.get() - '0';
      } while (is_digit(stream_.peek()));

      return true;
    }

    return false;
  }

  bool parse_tags(std::map<std::string, std::optional<std::string>> &tags)
  {
    //
    // <tags>          ::= <tag> [';' <tag>]*
    //

    std::pair<std::string, std::optional<std::string>> tag;
    if (!parse_tag(tag))
      return false;

    tags.clear();
    tags.insert(tag);
    while (consume_if(';')) {
      if (!parse_tag(tag))
        return false;
      tags.insert(tag);
    }

    return true;
  }

  bool parse_tag(std::pair<std::string, std::optional<std::string>> &tag)
  {
    //
    // <tag>           ::= <key> ['=' <escaped value>]
    //

    if (!parse_key(tag.first))
      return false;

    tag.second.reset();
    if (consume_if('=')) {
      std::string value;
      if (!parse_escaped_value(value))
        return false;
      if (value.size() != 0)
        tag.second = value;
    }

    return true;
  }

  bool parse_key(std::string &key)
  {
    //
    // <key>           ::= [ '+' ] [ <vendor> '/' ] <sequence of letters, digits, hyphens ('-')>
    //

    // this breaks simple/constant-symbol lookahead parsing as there is no indication that tells us if the sequence of
    // the key actuall contains a vendor unless we parsed up to either the '/' or the next SPACE (is this correct?)

    if (any_of<'=', ';', ' '>(stream_.peek()))
      return false;

    key.clear();
    do
      key += stream_.get();
    while (none_of<'=', ';', ' '>(stream_.peek()));

    return true;
  }

  bool parse_escaped_value(std::string &escaped_value)
  {
    //
    // <escaped value> ::= <sequence of any char_typeacters except NUL, CR, LF, semicolon (`;`) and SPACE>
    //

    escaped_value.clear();
    while (none_of<'\0', '\r', '\n', ';', ' '>(stream_.peek())) {
      char_type ch = stream_.get();
      if (ch == '\\') {
        switch (char_type seq = stream_.get(); seq) {
        case ':':ch = ';';
          break;

        case 's':ch = ' ';
          break;

        case '\\':ch = '\\';
          break;

        case 'r':ch = '\r';
          break;

        case 'n':ch = '\n';
          break;
        }
      }
      escaped_value += ch;
    }

    return true;
  }

  //
  // <vendor>        ::= <host>
  //
  //

  bool parse_host(std::string &host)
  {
    //
    // host       =  hostname / hostaddr
    //
    // Note: this needs adaption for hostnames starting with a digit!

    //if (is_letter(stream_.peek()))
    return parse_hostname(host);
/*
    if (is_digit(stream_.peek()))
      return parse_hostaddress(host);

    return false;
    */
  }

  bool parse_hostname(std::string &hostname)
  {
    //
    // hostname   =  name *( "." name )
    //
    // Note:
    // This implementation is not standard compilant. The first part of the hostname is relaxted to match the user
    // spec rather than the name spec to allow for use with the twitch specific prefix style:
    // :<user>!<user>@<user>.tmi.twitch.tv

    if (!parse_nickname(hostname))
      return false;

    while (consume_if('.')) {
      std::string name;

      if (!parse_name(name))
        return false;
      hostname += "." + name;
    }

    return true;
  }

  bool parse_name(std::string &name)
  {
    //
    // name = <let>[*[<let-or-digit-or-hyphen>]<let-or-digit>]
    //
    // Note:
    // This implementation violates the specification which is somehow shit. The spec requires name/shortname to allow
    // a digit as a first char_typeacter which would mean any HostAddr is also a HostName, which is somehow silly:
    //   shortname  =  letter *( letter / digit / "-" ) *( letter / digit )
    if (!is_letter(stream_.peek()))
      return false;

    name.clear();
    bool termination_ok;
    do {
      termination_ok = is_letter(stream_.peek()) || is_digit(stream_.peek());
      name += stream_.get();
    } while (is_letter(stream_.peek()) || is_digit(stream_.peek()) || stream_.peek() == '-');

    return termination_ok;
  }

  bool parse_hostaddress(std::string &hostaddress)
  {
    //
    // hostaddr   =  ip4addr / ip6addr
    //
    // Note:
    // This has not been implemented yet.
    return false;
  }

  bool parse_ipv4address(std::string &ipv4address)
  {
    //
    // ip4addr    =  1*3digit "." 1*3digit "." 1*3digit "." 1*3digit
    //
    // Note:
    // This has not been implemented yet. See parse_hostaddress.
    return false;
  }

  bool parse_ipv6address(std::string &ipv6address)
  {
    //
    // ip6addr    =  1*hexdigit 7( ":" 1*hexdigit )
    // ip6addr    =/ "0:0:0:0:0:" ( "0" / "FFFF" ) ":" ip4addr
    //
    // Note:
    // This has not been implemented yet. See parse_hostaddress.
    return false;
  }


  bool parse_prefix(std::string &prefix)
  {
    //
    // prefix     =  servername / ( nickname [ [ "!" user ] "@" host ] )
    //
    // Note:
    // Whilst the prefix does not contain the trailing SPACE required in the message_type grammar, this is used as a
    // criteria here. However it is not extracted here but in parsemessage_type().
    // TODO: Replace the subparsers by code invoked in context of the current instance.

    std::string data;
    while (stream_.peek() != ' ')
      data += stream_.get();

    {
      message_parser parser{data};
      if (parser.parse_host(prefix) && parser.eof())
        return true;
    }

    {
      message_parser parser{data};
      if (parser.parse_useridentifier(prefix) && parser.eof()) {
        return true;
      }
    }

    return false;
  }

  bool parse_useridentifier(std::string &prefix)
  {
    prefix.clear();
    if (!parse_nickname(prefix))
      return false;

    if (consume_if('!')) {
      std::string user;
      if (!parse_user(user))
        return false;
      prefix += '!' + user;
    }

    if (consume_if('@')) {
      std::string host;
      if (!parse_host(host))
        return false;
      prefix += '@' + host;
    }

    return true;
  }

  bool parse_nickname(std::string &nickname)
  {
    //
    // nickname   =  ( letter / special ) *8( letter / digit / special / "-" )
    //
    // Note:
    // This implementation violates the specification. The  minimum length of 9 char_types is not enforced.
    // Note:
    // This implementation violates the spec. It allows nicknames to start with a digit to be twitch compatible.

    if (!(is_letter(stream_.peek()) || is_special(stream_.peek()) || is_digit(stream_.peek())))
      return false;

    nickname.clear();
    do
      nickname += stream_.get();
    while (is_letter(stream_.peek())
           || is_special(stream_.peek())
           || is_digit(stream_.peek())
           || stream_.peek() == '-');

    return true;
  }

  bool parse_user(std::string &user)
  {
    //
    //  user       =  1*( %x01-09 / %x0B-0C / %x0E-1F / %x21-3F / %x41-FF )
    //             ; any octet except NUL, CR, LF, " " and "@"
    //
    if (any_of<'\0', '\r', '\n', ' ', '@'>(stream_.peek()))
      return false;

    user.clear();
    do
      user += stream_.get();
    while (none_of<'\0', '\r', '\n', ' ', '@'>(stream_.peek()));

    return true;
  }

  bool is_nospcrlfcl(char ch) const
  {
    // nospcrlfcl =  %x01-09 / %x0B-0C / %x0E-1F / %x21-39 / %x3B-FF
    //  ; any octet except NUL, CR, LF, " " and ":"

    return none_of<'\0', '\r', '\n', ' ', ':'>(ch);
  }

  template<char_type C, char_type... Cs>
  bool any_of(char_type ch) const
  {
    if (C == ch)
      return true;
    if constexpr (sizeof...(Cs) != 0)
      return any_of<Cs...>(ch);
    return false;
  }

  template<char_type... Cs>
  bool none_of(char_type ch) const
  {
    return !any_of<Cs...>(ch);
  }

  template<char_type A, char_type B>
  bool between(char_type ch) const
  {
    return A <= ch && ch <= B;
  }

  bool is_digit(char_type ch) const
  {
    return between<'0', '9'>(ch);
  }

  bool is_letter(char_type ch) const
  {
    return between<'a', 'z'>(ch) || between<'A', 'Z'>(ch);
  }

  bool is_hex_digit(char_type ch) const
  {
    return is_digit(ch) || between<'a', 'f'>(ch) || between<'A', 'F'>(ch);
  }

  bool is_special(char_type ch) const
  {
    return any_of<'[', ']', '\\', '`', '_', '^', '{', '|', '}'>(ch)
           || between<0x5B, 0x60>(ch)
           || between<0x7B, 0x7D>(ch);
  }

  bool consume_if(char_type ch)
  {
    if (char_type cur = stream_.peek(); cur != ch)
      return false;
    stream_.get();
    return true;
  }

  bool eof() const
  {
    return stream_.eof();
  }

private:
  std::istringstream stream_;
};

}
