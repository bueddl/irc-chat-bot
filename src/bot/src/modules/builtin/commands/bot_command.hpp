//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "../../../core/module/command.hpp"
#include "../../../twitch/usertype_authorizer.hpp"

namespace modules::builtin::commands
{

template<typename Module, typename Bot>
class bot_command : public core::module::command<bot_command<Module, Bot>, Module>
{
public:
  using bot_type = Bot;

  explicit bot_command(Module &module, bot_type &bot)
    : core::module::command<bot_command<Module, Bot>, Module>{module},
      bot_{bot}
  { }

  std::string_view get_name() const override
  {
    return "bot";
  }

  bool enable() override
  {
    this->template add_syntax(
      "version",
      &bot_command::display_version);

    this->template add_syntax(
      "commands",
      &bot_command::display_commands);

    return true;
  }

private:
  bot_type &bot_;

  bool display_version(const core::irc::context &context)
  {
    std::ostringstream message;
    message << "currently running oeo_bot in version "
            << bot_type::version;

    context.channel_message_to_user(message);
    return true;
  }

  bool display_commands(const core::irc::context &context)
  {
    context.channel_message_to_user("You can find all the supported MrDestructoid commands on https://bot.oneengineout.com");
    return true;
  }
};

}
