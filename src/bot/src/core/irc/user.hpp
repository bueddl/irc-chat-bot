//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "protocol/message.hpp"
#include "client.hpp"

#include <string>

namespace core::irc
{

using command = protocol::command;

class user
{
public:
  user(client_base &cl, std::string login, std::string display_name)
    : client_{cl},
      login_{std::move(login)},
      display_name_{std::move(display_name)}
  { }

  const std::string &login() const
  { return login_; }

  const std::string &display_name() const
  { return display_name_; }

  friend bool operator==(const user &lhs, const user &rhs)
  {
    return lhs.login_ == rhs.login_;
  }

  friend bool operator!=(const user &lhs, const user &rhs)
  {
    return !(lhs == rhs);
  }

private:
  client_base &client_;
  const std::string login_;
  const std::string display_name_;
};

struct hash
{
  std::size_t operator()(const user &user) const noexcept
  {
    return std::hash<std::string>{ }(user.login());
  }
};

};
