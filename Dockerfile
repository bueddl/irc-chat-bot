FROM conanio/clang11:latest

RUN sudo apt-get update && \
    sudo apt-get -y install libcurl4-openssl-dev libboost-dev libboost-system-dev rapidjson-dev

COPY src /sources
RUN mkdir /home/conan/build
WORKDIR /home/conan/build
RUN cmake -DCMAKE_BUILD_TYPE=Debug /sources
RUN cmake --build .
RUN sudo $(which cmake) --install .

ENV LD_LIBRARY_PATH=/usr/local/lib
ENTRYPOINT /usr/bin/ircpp
