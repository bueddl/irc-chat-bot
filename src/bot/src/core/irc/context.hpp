//
//  irc++ - A C++ IRC Chat Bot Framework
//  Copyright (C) 2017-2018  Sebastian Büttner <sebastian@bueddl.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.


#pragma once

#include "user.hpp"
#include "channel.hpp"

namespace core::irc
{

class context
{
public:
  explicit context(channel channel, user user, protocol::message const& message)
    : channel_{std::move(channel)},
      user_{std::move(user)},
      message_{message}
  { }

  const user &get_user() const
  { return user_; }

  const channel &get_channel() const
  { return channel_; }

  void channel_message(const std::string &text, bool colorize = false) const
  {
    channel_.message(text, colorize);
  };

  void channel_message(const std::ostringstream &stream, bool colorize = false) const
  {
    channel_.message(stream, colorize);
  }

  void channel_message_to_user(const std::string &text, bool colorize = false) const
  {
    channel_.message_to_user(text, user_, colorize);
  };

  void channel_message_to_user(const std::ostringstream &stream, bool colorize = false) const
  {
    channel_.message_to_user(stream, user_, colorize);
  }

  protocol::message const& original_message() const
  { return message_; }

private:
  channel channel_;
  user user_;
  protocol::message const& message_;
};

}
